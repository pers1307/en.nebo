var createPrettyPhoto = function() {
    $('.gallery a.not(.back)').each(function(){
        $(this).attr('rel', 'gallery[pp_gal]').prettyPhoto();
    });

    $("a[rel^='gallery']").prettyPhoto({
        custom_markup: '<div class="img"></div>'
    });
}

$(document).ready(function(){

    $('.sployler span.title').click(function() {
        $(this).parent().children('.fulltext').slideToggle('slow');
    })

    jQuery('a.btn_ar').click(function() {
        str = jQuery(this).val();
        jQuery.scrollTo("#"+str, 500);
    })

    $("#fb_popup").prettyPopin({
        loader_path: '/i/prettyPhoto/loader.gif'
    });

    // format galleries
    $('a[rel^=gallery]').each(function(){
        var $this = $(this);
        var gal_id = $this.attr('rel').split('/');

        if (gal_id[1]) {
            $.get('/-virtual-handler/galleries/', {id: gal_id[1]}, function(data){
                $this.replaceWith(data);
                createPrettyPhoto();
            });
        }
    });
});
