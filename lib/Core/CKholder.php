<?php
require_once 'Vars.php';

class CKholder extends Vars {

	function getEditor($instance, $value, $toolbar='', $width='100%', $height='200') {
		require_once ROOT.'_bo/ckeditor/ckeditor.php';
		require_once ROOT.'_bo/ckfinder/ckfinder.php';
		$CKEditor = new CKEditor();
		CKFinder::SetupCKEditor($CKEditor, 'ckfinder/');
		$config['skin'] = 'v2';
		if(!empty($toolbar)) $config['toolbar'] = $toolbar;
		$config['width'] = $width;
		$config['height'] = $height;
		$CKEditor->returnOutput = true;
		return $CKEditor->editor($instance, $value, $config);
	}

}
?>