<?php

class Log {
	private static $instance = null;

	public static function GetInstance() {
		if (is_null(self::$instance)) self::$instance = new Log();
		return self::$instance;
	}

	public static function write($msg, $exit=false, $level='notice') {
		$s = "\n[".date('Y-m-d H:i:s.u')."] [".$level."] ".$msg."\n";
		if ($exit) exit ($s);
		else echo $s;
	}

	public function __invoke($msg, $exit = false, $level = 'notice') {
		self::write($msg, $exit, $level);
	}
}

?>
