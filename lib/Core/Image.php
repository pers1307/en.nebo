<?
/**
 * @author Mayer Roman (majer.rv@gmail.com)
 * @see Рома - молодец
 */

class Image {

	private $image, $size, $handle;
    public $type, $ext, $width, $height;

    static function getType($type) {
		static $typesTable = array(
			IMAGETYPE_GIF	=> 'gif',
			IMAGETYPE_JPEG	=> 'jpeg',
			IMAGETYPE_PNG	=> 'png',
		);
		if (array_key_exists($type, $typesTable)) return $typesTable[$type];
		else return false;
	}

	/**
	 * @param $filePath
     * @return Image
	 */
	static function CreateFromFile($filePath) {
		$size = getimagesize($filePath);
		if (!$size) return false;
		$type = $size[2];
		$ext = self::getType($type);
		if (!$ext) return false;
		$createFunction = "imagecreatefrom$ext";
		if (!function_exists($createFunction)) return false;
		$resource = $createFunction($filePath);
		return new self($resource, $type, $ext, array($size[0], $size[1]));
	}

	static function Create($width, $height, $trueColor = true) {
		$resource = $trueColor ? imagecreatetruecolor($width, $height) : imagecreate($width, $height);
		$type = $trueColor ? IMAGETYPE_JPEG : IMAGETYPE_GIF;
		$ext = self::getType($type);
		return new self($resource, $type, $ext);
	}

	/**
	 * @param resource image $handle
	 * @param Int $type
	 * @param String $ext
     * @return Image
	 */
    public function __construct($handle, $type, $ext=null, $sizes=null) {
		$this->handle = $handle;
		$this->type = $type;
		$this->ext = is_null($ext) ? self::getType($type) : $ext;
		$this->width = !empty($sizes) && isset($sizes[0]) ? $sizes[0] : 0;
		$this->height = !empty($sizes) && isset($sizes[1]) ? $sizes[1] : 0;

//		if ($type == IMAGETYPE_PNG) $this->savealpha(true);
	}


	/**
	 * @param int $width ширина генерируемого изображения
	 * @param int $height высота генерируемого изображения (null = пропорционально)
	 * @param hex $rgb цвет фона, по умолчанию - белый
	 * @param int $quality качество генерируемого JPEG, по умолчанию - максимальное
	 */
    public function resize_fill($width, $height = null, $rgb = 0xFFFFFF, $quality = 100) {
		$this->setSize();

		$w = $this->width;
		$h = $this->height;

		$format = $this->ext;
		$icfunc = "imagecreatefrom$format";
		if (!function_exists($icfunc)) return false;

		$x_ratio = $width / $w;
		if (is_null($height)) {
			$k = $w / $width;
			$height = (int) $h / $k;
		}
		$y_ratio		=	$height / $h;
		$ratio			=	min($x_ratio, $y_ratio);
		$use_x_ratio	=	($x_ratio == $ratio);

		$new_width		=	$use_x_ratio  ? $width  : floor($w * $ratio);
		$new_height		=	!$use_x_ratio ? $height : floor($h * $ratio);
		$new_left		=	$use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
		$new_top		=	!$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

		$idest = imagecreatetruecolor($width, $height);
		imagefill($idest, 0, 0, $rgb);
		imagecopyresampled($idest, $this->handle, $new_left, $new_top, 0, 0, $new_width, $new_height, $w, $h);

		$this->handle = $idest;
		$this->setSize();
	}


	/**
	 * @param int $width ширина генерируемого изображения
	 * @param int $height высота генерируемого изображения (null = пропорционально)
	 * @param int $quality качество генерируемого JPEG, по умолчанию - максимальное
	 */
    public function resize($width, $height = null, $quality = 100) {
		$this->setSize();

		$w = $this->width;
		$h = $this->height;

		$format = $this->ext;
		$icfunc = "imagecreatefrom$format";
		if (!function_exists($icfunc)) return false;

		$x_ratio = $width / $w;
		if (is_null($height)) {
			$k = $w / $width;
			$height = (int) $h / $k;
		}

		if (($this->width < $width) or ($this->height < $height)) {
			return;
		}

		$y_ratio		=	$height / $h;
		$ratio			=	min($x_ratio, $y_ratio);
		$use_x_ratio	=	($x_ratio == $ratio);
		$new_width		=	$use_x_ratio  ? $width  : floor($w * $ratio);
		$new_height		=	!$use_x_ratio ? $height : floor($h * $ratio);
		$new_left		=	$use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
		$new_top		=	!$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

		$idest = imagecreatetruecolor($new_width, $new_height);
		imagecopyresampled($idest, $this->handle, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

		$this->handle = $idest;
		$this->setSize();
	}


	/**
	 * @param int $width
	 * @param int $height
	 * @param string $shift = top, left, right, bottom
	 * @param int $quality
	 * @param temp $s
     * @return null
	 */
    public function resize_crop($width, $height, $shift = false, $quality = 100) {
		$this->setSize();

		$original_width = $this->width;
		$original_height = $this->height;

		$format = $this->ext;
		$icfunc = "imagecreatefrom$format";
		if (!function_exists($icfunc)) return false;

		$kw = $original_width / $width;
		$kh = $original_height / $height;

		if ($kh < $kw) {
			$k = $kh;
			$h = $original_height;
			$w = round($width * $k);
			if ($shift == 'left') $x = 0;
			elseif ($shift == 'right') $x = $original_width - $w;
			else $x = round(($original_width - $w) / 2);
			$y = 0;
		} else {
			$k = $kw;
			$w = $original_width;
			$h = round($height * $k);
			$x = 0;
			if ($shift == 'top') $y = 0;
			elseif ($shift == 'bottom') $y = $original_height - $h;
			else $y = round(($original_height - $h) / 2);
		}

		$dest = imagecreatetruecolor($width, $height);
		imagecopyresampled($dest, $this->handle, 0, 0, $x, $y, $width, $height, $w, $h);

		$this->handle = $dest;
		$this->setSize();
	}

		/**
	 * @param int $width ширина генерируемого изображения
	 * @param int $height высота генерируемого изображения (null = пропорционально)
	 * @param int $quality качество генерируемого JPEG, по умолчанию - максимальное
	 */
    public function resize_main($width = null, $height = null, $quality = 100) {
    	if(!$width && !$height) return false;

		$this->setSize();

		$w = $this->width;
		$h = $this->height;

		$format = $this->ext;
		$icfunc = "imagecreatefrom$format";
		if (!function_exists($icfunc)) return false;

		if (!is_null($width)) {
			if($width < $height && $this->height < 210) $height = 210;
			$x_ratio = $width / $w;
			if (is_null($height)) {
				$k = $w / $width;
				$height = (int) $h / $k;
			}
		} else {
			if($height < $width && $this->width < 240) $width = 240;
			$x_ratio = $height / $h;
			if(is_null($width)) {
				$k = $h /$height;
				$width = (int) $w / $k;
			}
		}

		if (($this->width < $width) or ($this->height < $height)) {
			return;
		}

		$y_ratio		=	$height / $h;
		$ratio			=	min($x_ratio, $y_ratio);
		$use_x_ratio	=	($x_ratio == $ratio);
		$new_width		=	$use_x_ratio  ? $width  : floor($w * $ratio);
		$new_height		=	!$use_x_ratio ? $height : floor($h * $ratio);
		$new_left		=	$use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
		$new_top		=	!$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

		$idest = imagecreatetruecolor($new_width, $new_height);
		imagecopyresampled($idest, $this->handle, 0, 0, 0, 0, $new_width, $new_height, $w, $h);

		$this->handle = $idest;
		$this->setSize();
	}

	private function setSize() {
		$this->width = imagesx($this->handle);// $this->x();
		$this->height = imagesy($this->handle); //$this->y();
	}

	public function waterMark($str, $color1 = 0xFFFFFF, $color2 = 0x000000) {
		$angle = 0;
		$this->setSize();
		$x = $this->width - 185;
		$y = $this->height - 10;
		$font = ROOT.'i/arial.ttf';

		imagefttext($this->handle, 10, $angle, $x+1, $y+1, $color2, $font, $str);
		imagefttext($this->handle, 10, $angle, $x, $y, $color1, $font, $str);
	}

	public function waterMarkImg($img) {
        if (!is_file($img) || !is_readable($img)) return false;
        $stamp = imagecreatefrompng($img);

        $margin_right = 50;
        $margin_bottom = 50;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);

        imagecopy($this->handle, $stamp, imagesx($this->handle) - $sx - $margin_right, imagesy($this->handle) - $sy - $margin_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
    }

    public function crop($x, $y, $w, $h) {
		$newImg = self::Create($w, $h);
		if (imagecopy($newImg->handle, $this->handle, 0, 0, $x, $y, $w, $h)) $this->handle = $newImg->handle;
		//imagedestroy($newImg);
	}

    public function save($path, $format = null, $quality = 100) {
		$method = is_null($format) ? "image{$this->ext}" : "image$format";
		if ($method == 'png') $quality = min(9, $quality);
		$this->$method($path, $quality, PNG_ALL_FILTERS);
	}

    public function __call($function, $arguments) {
		array_unshift($arguments, $this->handle);
		$imageFunction = $function;
		if (!function_exists($imageFunction)) $imageFunction = "images$function";
		if (function_exists($imageFunction)) {
			$retValue = call_user_func_array($imageFunction, $arguments);
			if (is_resource($retValue)) $retValue = new self($retValue);
			return $retValue;
		}
		else throw new Exception("Method '$function' not found");
	}

    public function clear() {
		$this->size = '';
		$this->width = '';
		$this->height = '';
		imagedestroy($this->handle);
	}

    public function getHandle() {
        return $this->handle;
    }

}
?>
