<?
class MyDB2 {

    private static $instance = null;

    public $db;
    public $lp = false;

    /**
     * @return MyDB2
     */
    public static function GetInstance() {
        if (is_null(self::$instance)) self::$instance = new MyDB2();
        return self::$instance;
    }

    function __construct() {
        $dsn = 'mysql://'.SQL_LOGIN.':'.SQL_PASSWD.'@'.SQL_HOST.'/'.SQL_DBASE;
        $db_options = array('persistent'=>false, 'debug'=>2);
        $db_lang = SQL_CHARSET;

        $this->db =& DB::connect($dsn, $db_options);
        if (PEAR::isError($this->db)) die($this->db->getMessage());
        $this->db->setFetchMode(DB_FETCHMODE_ASSOC);
        $this->db->query("SET NAMES {$db_lang}");
    }

    function getLastId() {
        $res = $this->db->getRow('select LAST_INSERT_ID() as last_id');
        if (PEAR::isError($res)) die($res->getMessage());
        return $res['last_id'];
    }

    function insert($tbl,$data) {
        $r = $this->db->autoExecute($tbl, $data, ($this->lp) ? DB_AUTOQUERY_INSERT_LP : DB_AUTOQUERY_INSERT);
        if (PEAR::isError($r)) {
            die($r->getDebugInfo());
        }
        return $this->getLastId();
    }

    function update($tbl,$data,$where) {
        $r = $this->db->autoExecute($tbl, $data, ($this->lp) ? DB_AUTOQUERY_UPDATE_LP: DB_AUTOQUERY_UPDATE, $where);
        if (PEAR::isError($r)) die($r->getDebugInfo());
        return !PEAR::isError($r);
    }

    function replace($tbl,$data,$where) {
        $row = $this->db->getRow('SELECT * FROM '.$tbl.' WHERE '.$where);
        if (empty($row)) {
            return $this->insert($tbl, $data);
        } else {
            $this->update($tbl, $data, $where);
        }
    }

    function delete($tbl, $where) {
        if (!empty($where)) $where = ' WHERE '.$where;
        $r = $this->db->query('delete from '.$tbl.$where);
        if (PEAR::isError($r)) die($r->getMessage());
        return PEAR::isError($r);
    }

    function mysql_enum_values($table, $field, $alpha_sort = false) {
        $row = $this->db->getRow('show columns from '.$table.' LIKE ?',array($field));
        if (PEAR::isError($row)) die($row->getMessage());
        if (!$alpha_sort) {
            return(explode("','", preg_replace("/.*\('(.*)'\)/", "\\1", $row["Type"])));
        } else {
            $ar = explode("','", preg_replace("/.*\('(.*)'\)/'", "\\1", $row["Type"]));
            natcasesort($ar);
            return $ar;
        }
    }

    function __call($method, $vars) {
        if (method_exists($this->db, $method)) {
            return call_user_func_array(array($this->db, $method), $vars);
        }
    }

    /*public function getAll($sql, $params=array()) {
        $debug = isset($_GET['debug']);
        if ($debug) {
            $time_start = $this->getmicrotime();
        }

        $res = $this->db->getAll($sql, $params);

        if ($debug) {
            if (count($params)) {
                $sql .= '<br>'.implode(', ', $params);
            }
            $time_end = $this->getmicrotime();
            $time = round($time_end - $time_start, 4);
            $c = (is_array($res)) ? count($res) : 0;
            if ($time >= 0.1) {
                echo "<span style=\"font-size: 10px; color:#000;\">[{$time} | {$c}] <i>{$sql}</i></span><br/>";
            } else {
                echo "<span style=\"font-size: 10px; color:#ccc;\">[{$time} | {$c}] <i>{$sql}</i></span><br/>";
            }
        }
        return $res;
    }*/

    private function getmicrotime() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }


    public function getIds($arr = array(), $key = 'id') {
        $ids = array();
        foreach ($arr as $row) {
            if (isset($row[$key])) {
                $ids[] = $row[$key];
            }
        }
        return implode(', ', $ids);
    }


    public function listData($list, $key, $val) {
        $output = array();
        foreach ($list as $row) {
            $output[$row[$key]] = $row[$val];
        }
        return $output;
    }

}