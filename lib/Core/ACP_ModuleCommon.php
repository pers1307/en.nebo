<?
class ACP_ModuleCommon extends ACP_Module {

	protected $name;
	protected $table;
	protected $sort;

	public function __construct($name, $table, $sort='id') {
		$this->table = $table;
		$this->name = $name;
		$this->sort = $sort;
		parent::__construct();
	}

	public function rewrite() {
		switch ($this->action) {
			case "edit":
			case 'save':
			case "delete":
				$this->{$this->action}();
				break;
 			default:
 				$this->lists();
		}
		return true;
	}

	public function lists() {
		$q = "SELECT * FROM {$this->table} ORDER BY {$this->sort} ASC";
		$items = $this->db->getAll($q);
		if (!PEAR::isError($items)) $this->smarty->assign('items', $items);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_list.tpl');
	}

	public function edit() {
		if ((isset($_GET['id'])) and ($id = intval($_GET['id']))) {
			$item = $this->db->getRow("SELECT * FROM {$this->table} WHERE id = {$id}");
		} else {
			$item = array();
		}

		$this->smarty->assign('item', $item);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_edit.tpl');
	}

	public function save() {
		if (empty($_POST)) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module");
			exit;
		}
		$id = intval($_POST['id']);
		$arr = $this->save_data();

		if ($id) $this->db->update($this->table, $arr, "id = $id");
		else $id = $this->db->insert($this->table, $arr);

		header("Location: /_bo/?doc_id={$this->doc_id}&module");
		exit;
	}

	public function delete(){
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$this->db->delete($this->table, "id = $id");
		echo 1;
		exit();
	}

	public function save_data() {
		$arr = array();
		return $arr;
	}

}