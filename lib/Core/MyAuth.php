<?
class MyAuth {

	var $c = 1;
	var $userList;
	var $authed;
	var $user;
	var $bo_auth_session = 'bo_auth';

	/**
	 * @var MyDB2
	 */
	var $db;

	/**
	 * @var Auth
	 */
	var $auth;


	function __construct() {
		$this->db = MyDB2::GetInstance();
	}


	function startAuth($showLogin = true) {
		require_once 'Auth.php';
		$vars = Vars::GetInstance();
		// auth init options
		$auth_options = array();
		$auth_options['dsn'] = $vars->get_DSN();
		$auth_options['table'] = 'users';
		$auth_options['usernamecol'] = 'login';
		$auth_options['passwordcol'] = 'passw';

		// auth object
		$this->auth = new Auth('DB', $auth_options, array($this, 'showUserLogin'));
		$this->auth->setSessionName($this->bo_auth_session);
		$this->auth->setShowLogin($showLogin);
		$this->auth->start();
		$this->authed = $this->auth->checkAuth();
		if ($this->authed) {
			if (isset($_REQUEST['username'])) {
				header('Location: ' . $_SERVER['REQUEST_URI']);
				exit;
			} else {
				$this->user = $this->getUserParams();
				$this->user_modules = explode(';', $this->user['bo']);
			}
		}
		if (!$this->authed && $showLogin) die('Вы не авторизованы');
		return $this->authed;
	}


	function showUserLogin($user, $auth) {
		$tpl = SmartyHolder::GetInstance(R_tpl_acp);
		$tpl->assign($_POST);
		$errors = '';
		if ($auth) {
			switch ($auth) {
				case -1:
					$errors = "Время бездействия в системе истекло";
					break;
				case -2:
					$errors = "Время работы в системе истекло";
					break;
				case -3:
					$errors = "Неправильные имя пользователя или пароль";
					break;
				case -4:
					$errors = "Ошибка авторизации";
					break;
				case -5:
					$errors = "Нарушение прав доступа";
					break;
			}
		}
		$tpl->assign('errors', $errors);
		$tpl->display('access.tpl');
		exit;
	}


	function extr($parent, $lvl) {
		$login = trim(str_replace(array('"', "'", '%', ' '), '', $_GET['login']));
		$uu = $this->getUserParams($login);
		if (empty($uu)) return "<span style=\"color:white\">&nbsp;&nbsp;Пользователь не найден</span>";

		$lvl ++;

		// level >3 killed :)
		if ($lvl>=2) return;

		$bg = "#ffffff";
		$ar = "";

		if ($lvl > 1) {
			$ar = "<img src=\"/_bo/images/arrow_level.gif\" width=\"10\" height=\"10\" alt=\"Level2\">";
		}

		$all = $this->db->getAll("SELECT id, fname, parent, rights FROM tree WHERE parent = $parent AND editable = 'y' ORDER BY priority, id");
		if (PEAR::isError($all)) {
			die($f->getMessage());
		}

		if (!empty($all)) {
			$out = '';
			// strange things happens here
			foreach ($all as $row) {
				$row['lvl'] = $lvl;
				$f=$this->db->getRow("SELECT head FROM tree WHERE id = {$row['id']}");
				if (PEAR::isError($f)) {
					die($f->getMessage());
				}

				$out .=  "<tr>";
				if ($lvl==1) $out .=  "<td>&nbsp;<b>". $ar ." ". $f['head']."</b></td>";
				else $out .=  "<td>".str_repeat("&nbsp;&nbsp;", $lvl) . "$ar " . $f['head']."</td>";
				$out .=  '<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="4" class="pxt">
									<tr>
										<td class="txt" align=center><b>Доступность</b></td>
									</tr>';

				$mode = "";

				if (!empty($row['rights'])) {
					$rr = explode(";", $row['rights']);
					foreach ($rr as $rt) {
						if ( empty($rt) ) continue;
						if ( !preg_match("/^".$uu['uid']."\(/", $rt) ) continue;
						else {
							list($jnk, $mode) = split("\(", $rt);
							$mode = (int) $mode;
						}
					}
				}

				if ($mode == '11') {
					$c = "checked";
					$c2 = "";
				} else {
					$c2 = "checked";
					$c = "";
				}
				$out .=
						"<tr>
							<td class=\"txt\" align=center>
							<input type='radio' name=\"".$row['id']."_item_".$uu['uid']."\" ".$c." value='11' /> Да&nbsp;&nbsp;<br/>
							<input type='radio' name=\"".$row['id']."_item_".$uu['uid']."\" ".$c2." value='0'/> Нет
							</td>
						</tr>"
						;
				unset($mode, $c, $c2);

				$out .=  "
							</td>";

				if ($lvl>1) {
					$out .=  	"<td align=center>
									наследовать<br>права<br>
									<input type=\"checkbox\" name=\"heritage_$row[id]\">
								</td>";
				}
				$out .=  "</tr></table>
					</td>";

				$out .=  "</tr>";
				unset($db2, $f);

				$children = $this->extr($row['id'], $lvl);
				$out .= $children;
			}
			return $out;
		}
	}


	function buildAccessTable($login) {
		if (empty($login)) $login = Utils::parseGet('login', 0);
		$uu = $this->getUserParams($login);

		$out = "";
		$out .= '<form method="post" action="?action=saveUserDivs&login='.$login.'">';
		$out .= '<table width="100%" border="0" cellspacing="1" cellpadding="0" class="pxt">';
		$out .= $this->extr(0, 0);
		$out .= '</table>';
		$out .= '<p><input type="submit" value="Сохранить"></p>';
		$out .= '</form>';

		return $out;
	}


	function unpackMode($mode) {
		switch ($mode)
		{
			case 1:
				$mode = "e";
				break;

			case 3:
				$mode = "d";
				break;

			case 7:
				$mode = "c";
				break;

			case 4:
				$mode = "ed";
				break;

			case 8:
				$mode = "ec";
				break;

			case 10:
				$mode = "dc";
				break;

			case 11:
				$mode = "edc";
				break;
		}
		return $mode;
	}


	function checkAccess($id, $action='e') {
		if (empty($id) && $action != 'c') return false;
		if (empty($id) && $action == 'c') $id = Utils::parsePost('parent',1);
		$user = $this->getUserParams();

		if ($user['status'] == 'admin') return true;

		$row = $this->db->getRow("SELECT rights FROM tree WHERE id = $id");

		if (PEAR::isError($row)) return false;

		if ( empty($row['rights']) ) return false;

		$rr = explode(";", $row['rights']);

		foreach ($rr as $rt) {
			if ( empty($rt) ) continue;
			if ( !preg_match("/^".$user['uid']."\(/", $rt) ) continue;
			else
			{
				list(, $mode) = preg_split("/\(/", $rt);
				$mode = (int) $mode;
				$mode = $this->unpackMode($mode);
				if ( preg_match("/$action/", $mode) ) return true;
				else return false;
			}
		}
	}


	function getUsers() {
		$out = array();
		// получаем список всех пользователей
		$out = $this->db->getAll("SELECT * FROM tree");
		if (PEAR::isError($out)) $out=false;
		$this->userList = $out;
		return $out;
	}


	function trMode($mode) {
		if ($mode == 'edit') return 1;
		if ($mode == 'del') return 3;
		if ($mode == 'create') return 7;
	}


	function unpackAccessUpdate() {
		$access = array();
		$heritage = array();
 		foreach ($_POST as $kk => $vv) {
			if (preg_match("/^\d/", $kk)) // если указаны права для конкретного раздела
			{
				list($dd,$mode,$vvv) = explode("_", $kk);
				// dd - id раздела
				// vvv - uid юзверга
				if ( array_key_exists($dd, $access) && array_key_exists($vv, $access[$dd]) ) $access[$dd][$vvv] += $vv;
				else $access[$dd][$vvv] = $vv;
			} elseif (eregi("heritage", $kk)) {
				list($jnk, $dd) = explode("_", $kk);
				$heritage[$dd] = 1;
			}
		}

		foreach ($access as $id => $uu) {
			$old_rights = $this->db->getOne("SELECT rights FROM tree WHERE id=$id");
			foreach ($uu as $uid => $mode) {
				if ($mode==0) {
					$rights = str_replace($uid."(11);","",$old_rights);
				} elseif($mode==11) {
					if (preg_match("/$uid\(11\);/",$old_rights)) {
						$rights = $old_rights;
					} else {
						$rights = $old_rights.$uid."(11);";
					}
				}
			}

			$this->db->update('tree', compact('rights'), "id = $id");
			unset($rights);
		}

		foreach ($heritage as $id => $jnk) {
			$a = $this->db->getRow("SELECT parent FROM tree WHERE id = $id");
			if (PEAR::isError($a)) {
				die($a->getMessage());
			}

			$parent = $a['parent'];
			if ($parent == 0) continue;

			$a=$this->db->getRow("SELECT rights FROM tree WHERE id = $parent");
			if (PEAR::isError($a)) {
				die($a->getMessage());
			}
			$rights = $a['rights'];

			$this->db->update('tree', compact('rights'), "id = $id");

			unset($a, $rights, $parent);
		}
	}


	function getUserParams($login = '') {
		if (empty($login) || $login == ':auto:') {
			$login = $this->auth->getUsername();
		}

		$u = $this->db->getRow("SELECT * FROM users WHERE login = '{$login}'");
		if (PEAR::isError($u)) {
			die($u->getMessage());
		}

		return $u;
	}


	function clearAuth() {
		unset($_SESSION['auth']);
	}


	function checkAdmin() {
		$r = $this->getUserParams();
		if (!empty($r) && $r['status'] == 'admin') {
			return true;
		} else {
			return false;
		}
	}

}
?>
