<?
class ImplodeJS {

	/**
	 * @var MyDB2
	 */
	var $db;
	
	private $ext = '.js';
	
	
	function __construct() {
		$this->db = MyDB2::GetInstance();
	}


	/**
	 * @return string
	 */
	public static function Get($path, $var_name) {
		$obj = new ImplodeJS();
		$file_name = $obj->confirm($path, $var_name);
		return $file_name;
	}


	private function confirm($path, $var_name) {
		if (!$path or !$var_name) return false;
		if (!file_exists($path) or !is_dir($path)) return false;
		@chmod($path, 0777);
		$file = $this->db->getOne("SELECT value FROM vars WHERE name = '{$var_name}'");
		if (PEAR::isError($file)) return false;
		
		if ($file) {
			$imlode_file = $path.$file;
			if (file_exists($imlode_file)) {
				return $file;
			}
		}
		
		$new_name = $var_name.'-'.dechex(time()).$this->ext;
		$saved = $this->implode($path, $path.$new_name);
		if (!$saved) {
			$new_name = '';
		}
		if (!$file) {
			$this->db->insert('vars', array('value'=>$new_name, 'name'=>$var_name, 'visibility'=>0));
		} else {
			$this->db->update('vars', array('value'=>$new_name, 'visibility'=>0), "name = '{$var_name}'");
		}
		return $new_name;
	}


	private function implode($path, $new_file) {
		$d = dir($path);
		$content = '';
		while (false !== ($entry = $d->read())) {
			if (($entry == '.') or ($entry == '..')) continue;
		    $item = $path.$entry;
		    
		    if (is_dir($item)) continue;
		    if (substr($entry, 0, 1) == '!') continue;
		    
		    $e = strrpos($entry, '.');
			if (!$e) continue;
			$ext = substr($entry, $e);
			if ($ext != $this->ext) continue;
		    
		    $content .= file_get_contents($item);
//		    $content .= "\r\n\r\n";
		    $content .= "\r\n\r\n//-------------------------------------------------------------------------------------\r\n\r\n";
		}
		$d->close();
		if ($content) {
			$res = @file_put_contents($new_file, $content);
			return ($res) ? true : false;
		}
		return false;
	}

}