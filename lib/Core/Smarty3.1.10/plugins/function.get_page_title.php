<?
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
function smarty_function_get_page_title($params, &$smarty) {
	$page = Page::GetInstance();
	if (isset($params['sep'])) {
		$page->show_title($params['sep']);
	} else {
		$page->show_title();
	}
}
?>