<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty russian month name
 *
 * @param integer
 */
function smarty_modifier_rumonth2($n) {
//	return my_ucfirst(Utils::ruMonth2($n));
	return Utils::ruMonth2($n);
}


function my_ucfirst($string, $e ='utf-8') {
	if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string))
	{
		$string = mb_strtolower($string, $e);
		$upper = mb_strtoupper($string, $e);
		preg_match('#(.)#us', $upper, $matches);
		$string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
	} else {
		$string = ucfirst($string);
	}
	return $string;
}