<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
/**
 * Smarty russian month name
 *
 * @param integer
 */

function smarty_modifier_rumonth($n) {
	return Utils::ruMonth($n);
}

?>