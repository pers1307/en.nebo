<div class="padding-block">
    <h3>Список вакансий</h3>
    <a class="btn btn-success" href="?doc_id={$doc_id}&module&action=vacancy_edit">
        <i class="icon-plus icon-white"></i>
        Добавить вакансию
    </a>
</div>
    <form action="/_bo/" id="arport_select" class="form-horizontal" method="get">
        <input type="hidden" name="doc_id" value="{$doc_id}"/>
        <input type="hidden" name="module" value=""/>
        <div class="control-group">
            <label class="control-label">Фильтр по аэропорту</label>
            <div class="controls">
                <select name="airport" id="airport" onchange="$('#arport_select').submit()">
                    <option value="">Все</option>
                    {foreach $airports as $airport}
                        <option value="{$airport.id}" {if $smarty.get.airport == $airport.id}selected {/if}>{$airport.title}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </form>

{if !empty($vacancies)}
    <table class="table table-striped">
        <tr>
            <th style="width: 20px;">#</th>
            <th>Название вакансии</th>
            <th style="width: 100px;">&nbsp;</th>
            <th style="width: 90px;">&nbsp;</th>
        </tr>
        {foreach $vacancies as $item}
            <tr id="tr_{$item.id}"{if $item.enabled == 0} class="info muted"{/if}>
                <td><a href="?doc_id={$doc_id}&module&action=vacancy_edit&id={$item.id}"{if $item.enabled == 0} class="info muted"{/if}>{$item.id}</a></td>
                <td><a href="?doc_id={$doc_id}&module&action=vacancy_edit&id={$item.id}"{if $item.enabled == 0} class="info muted"{/if}>{$item.title}</a></td>
                <td class="center">
                    {if $item.resumes}
                    <a href="?doc_id={$doc_id}&module&action=resume_list&vacancy={$item.id}" class="btn btn-mini btn-info">
                        <i class="icon-list-alt icon-white"></i> Резюме ({$item.resumes})
                    </a>
                    {/if}
                </td>
                <td class="center"><span onclick="news_delete('{$item.id}','{$item.title}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td>
            </tr>
        {/foreach}
    </table>

{else}
    <div class="padding-block"><p>Вакансии не созданы</p></div>
{/if}

<script type="text/javascript">
    function news_delete(id, title) {
        if (confirm('Удалить "'+title+'"?')) {
            $.get('?doc_id='+doc_id+'&module&action=vacancy_delete', { id:id }, function(data) {
                if (data.success) {
                    $('#tr_'+id).fadeOut('fast');
                }
            }, 'json')
        }
    }
</script>