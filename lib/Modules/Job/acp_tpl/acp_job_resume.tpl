<div class="padding-block" xmlns="http://www.w3.org/1999/html">
    <h3>Резюме для вакансии &laquo;{$item.title}&raquo;</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку вакансий
    </a>
</div>

{if $resumes}
    <table class="table table-striped">
        <tr>
            <th>ФИО</th>
            <th>Контактная информация</th>
            <th>Комментарий</th>
            <th style="width: 120px;">&nbsp;</th>
            <th style="width: 80px;">&nbsp;</th>
        </tr>
        {foreach $resumes as $k => $resume}
        <tr>
            <td>{$resume.name}</td>
            <td>{$resume.contact}</td>
            <td>{$resume.comment}</td>
            <td>
                {if $resume.hash}
                    <a href="/career/vacancy/?resume={$resume.hash}" class="btn btn-mini btn-success"><i class="icon-download icon-white"></i> Скачать резюме</a>
                {/if}
            </td>
            <td><a href="?doc_id={$doc_id}&module&action=resume_delete&id={$resume.id}" class="deleteResume btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</a></td>
        </tr>
        {/foreach}


{*
    <div class="panel-group" id="accordion">
        {foreach $resumes as $k => $resume}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#res{$k}">
                            {$resume.name} - {$resume.contact}
                        </a>
                    </h4>
                </div>
                <div id="res{$k}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>{$resume.comment}</p>
                        {if $resume.url}
                            <a href="{$resume.url}">Скачать резюме</a>
                        {/if}
                        <a class="deleteResume" href="?doc_id={$doc_id}&module&action=resume_delete&id={$resume.id}">Удалить резюме</a>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
    *}
{else}
        <tr>
            <td colspan="4">Резюме не найдены</td>
        </tr>
    </table>
{/if}

<script type="application/javascript">
    $(function(){
        $(document).on('click', '.deleteResume', function(e) {
            e.preventDefault();
            if (confirm('Удалить резюме?')) {
                $.get(e.target.href, function(data) {
                    if (data.success)
                        $(e.target).closest('.panel').remove();
                }, 'json')
            }
        })
    })
</script>