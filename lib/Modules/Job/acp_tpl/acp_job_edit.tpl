<div class="padding-block" xmlns="http://www.w3.org/1999/html">
    <h3>{if $item.id}Вакансия &laquo;{$item.title}&raquo;{else}Новая вакансия{/if}</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку вакансий
    </a>
</div>

<form method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=vacancy_edit&id={$item.id}" class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="title">Название</label>
        <div class="controls">
            <input type="text" class="span5" id="title" name="title" value="{$item.title}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="preview">Краткое описание</label>
        <div class="controls">
            <textarea name="preview" id="preview" class="span5">{$item.preview}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="text">Полное описание</label>
        <div class="controls">
            <textarea name="text" id="text">{$item.text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="salary">Зарплата</label>
        <div class="controls">
            <input type="text" class="span1" id="salary" name="salary" value="{$item.salary}" autocomplete="off" /> руб.
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="experience">Опыт работы</label>
        <div class="controls">
            <input type="text" name="experience" id="experience" value="{$item.experience}" class="span1" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="schedule">График</label>
        <div class="controls">
            <select name="schedule" id="schedule" class="span2">
                {foreach $schedule as $key => $type}
                    <option value="{$key}">{$type}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="airport_id">Аэропорт</label>
        <div class="controls">
            <select name="airport_id" id="airport_id" value="{$item.airport_id}" class="span2">
                {foreach $airports as $airport}
                    <option value="{$airport.id}" {if $airport.id == $item.airport_id}selected{/if}>{$airport.title}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="contact">Контактное лицо</label>
        <div class="controls">
            <textarea name="contact" id="contact" class="span5">{$item.contact}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="enabled">Открыта</label>
        <div class="controls">
            <input type="checkbox" name="enabled" id="enabled" value="1" {if $item.enabled}checked{/if} />
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-primary b-save">Сохранить</button>
        </div>
    </div>
</form>

<script type="application/javascript">
    CKEDITOR.replace('text', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "250" });

</script>