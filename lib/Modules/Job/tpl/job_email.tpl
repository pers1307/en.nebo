<html>
<head>
    {literal}
        <style>
            body		{ margin: 0; padding: 0; background-color: white; }
            *			{ font-size: 9pt; font-family: Tahoma, Arial, sans-serif; }
        </style>
    {/literal}
</head>
<body style="padding: 15px;">
    <h1>Отклик на вакансию {if !empty($title_vacancy)}&laquo;{$title_vacancy}&raquo;{/if}</h1>

    <table style="border: 1px solid #cccccc;" width="600" cellspacing="0" cellpadding="5">
        <tr>
            <td width="200">Имя отправителя:</td>
            <td>{$item.name|stripslashes}</td>
        </tr>
        <tr>
            <td>Контактные данные:</td>
            <td>{$item.contact|stripslashes}&nbsp;&nbsp;{if !empty($feedback.phone)}{$feedback.phone}{else}телефон не указан{/if}</td>
        </tr>
        <tr>
            <td>Комментарий:</td>
            <td>{$item.comment|stripslashes}</td>
        </tr>
        {if {$link}}
        <tr>
            <td>Резюме:</td>
            <td><a href="http://{$link}">Скачать резюме</a></td>
        </tr>
        {/if}
    </table>

    {*<a href="/_bo/?doc_id=15&module&action=vacancy_edit&id={$item.vacancy_id}">Все резюме для вакансии</a><br />*}

</body>
</html>