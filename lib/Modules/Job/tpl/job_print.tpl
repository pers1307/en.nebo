<!DOCTYPE html>
<html>
<head>

</head>
<body>
<div>
    <div>
        <h1>{$vacancy.title}</h1>

        <p>{$vacancy.city}</p>

        <p>Added: {$vacancy.createdAt}</p>

        <div>{$vacancy.salary|number_format:0:"":" "} RUB</div>
        <div>{$vacancy.schedule}</div>
        <div>Experience: {$vacancy.experience}</div>
        <p>Contacts:<br/>{$vacancy.contact}</p>
    </div>
    <div>
        {$vacancy.text}
    </div>
    <script type="application/javascript">
        window.print();
    </script>
</div>
</body>
</html>