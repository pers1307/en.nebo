<div class="job info">
    <a href="{$vacancy_link}?airport={$vacancy.airport_id}" class="back">Back</a>
    <div class="title">{$vacancy.title}</div>
    <div class="left">
        <p>{$vacancy.city}</p>
        <p>Added: {$vacancy.createdAt}</p>
        {if $vacancy.salary != 0}<div class="money">{$vacancy.salary|number_format:0:"":" "} RUB</div>{/if}
        <div class="day">{$vacancy.schedule}</div>
        <div class="other">Experience: {$vacancy.experience}</div>
        <a href="" class="resume">{if $errors}View details{else}Send CV{/if}</a><br /><br />
        <a href="/airports/{$vacancy.airport_id}">{$vacancy.airport}</a><br /><br />
        <p>Contact:<br />{$vacancy.contact|nl2br}</p>
    </div>
    <div class="right">
        <div class="vacancyText {if $errors}hide{/if}">
            {$vacancy.text}
            <p class="print">
                <a href="{$vacancy_link}?print={$vacancy.id}">Print</a>
            </p>
            <br class="clear" />
        </div>
        <div class="resumeForm {if !$errors}hide{/if}">
            <form id="resume_form" method="post" enctype="multipart/form-data">
                <div class="resume-form">
                    <div class="row">
                        <label class="valid" for="name">Full name</label>
                        <input type="text" name="name" id="name" class="name" value="{$smarty.post.name}">
                        {if $errors.name}
                            <div class="error">{$errors.name}</div>
                        {/if}
                    </div>

                    <div class="row">
                        <label class="valid" for="contact">Contact (phone, e-mail)</label>
                        <input type="text" name="contact" id="contact" class="contacts" value="{$smarty.post.contact}">
                        {if $errors.contact}
                            <div class="error">{$errors.contact}</div>
                        {/if}
                    </div>

                    <div class="row">
                        <label for="comment">Comment</label>
                        <textarea name="comment" id="comment" class="comment">{$smarty.post.comment}</textarea>
                    </div>

                    <div class="row">
                        <label for="file">CV (.doc, .docx, .pdf less than 2 MB)</label>
                        <input type="file" id="file" name="file" class="file-resume" />
                        {if $errors.file}
                            {foreach $errors.file as $error}
                                <div class="error">{$error}</div>
                            {/foreach}
                        {/if}
                    </div>

                    <div class="row">
                        <button type="submit" class="btn-send">Send</button>
                    </div>
                    <br class="clear" />
                </div>
            </form>
        </div>
    </div>
    <script type="application/javascript">
        $(function() {
            $('.resume').click(function(e) {
                e.preventDefault();
                if ($(this).text() == 'Send CV') {
                    $(this).text('To job description');
                } else {
                    $(this).text('Send CV');
                }
                $('.vacancyText, .resumeForm').toggleClass('hide');
            })
        })
    </script>

</div>