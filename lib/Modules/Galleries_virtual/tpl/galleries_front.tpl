{if ($fl_galleries)}
    <div class="gallery inner">
		{foreach from=$fl_galleries item=file name=thumb}
		    <div class="photo" {if $smarty.foreach.thumb.iteration %4 == 0}style="margin-right: 0"{/if}>
		        <a href="{$file.link}" title="{$file.description|htmlspecialchars}"><img alt="{$file.title|htmlspecialchars}" src="{if !empty($file.thumb)}{$file.thumb}{else}{$file.link}{/if}" /></a>
		    </div>
		{/foreach}
	</div>
{/if}