<?
class ACP_Presskit extends ACP_Module
{

    protected $name = __CLASS__;

    private $folder = 'presskit/';
    private $path;
    private $pageSize = 10;
    private $img_sizes = array(
        280 => '_thumb'
    );

    function __construct()
    {
        parent::__construct();
        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;
    }

    function rewrite()
    {
        switch ($this->action) {

            case 'delete':
                $this->delete();
                return false;
                break;

            case 'delete_category':
                $this->delete_category();
                return false;
                break;

            case 'edit':
                $this->edit();
                return false;
                break;

            case 'category':
                $this->category();
                break;

            case 'edit_category':
                $this->edit_category();
                return false;
                break;

            default:
                $this->index();
                break;

        }
        return true;
    }

    private function index()
    {
        $categories = $this->db->getAll("SELECT * FROM presskit_categories WHERE category_id IS NULL");

        $this->_index($categories);
    }

    private function category()
    {
        $id = Utils::parseGet('id');
        $category = $this->db->getRow("SELECT * FROM presskit_categories WHERE id = ?", array($id));
        if ($category['parent']) {
            $categories = $this->db->getAll("SELECT * FROM presskit_categories WHERE category_id = ?", array($id));
            $this->smarty->assign('category_id', $category['id']);
            $this->_index($categories);
        } else {
            $this->_items($category);
        }
    }

    private function _index($categories)
    {
        $path = $this->path;
        $folder = $this->folder;
        array_walk($categories, function(&$item) use ($path, $folder) {
            $file = "{$path}category_{$item['id']}.jpg";
            if (file_exists($file))
                $item['thumb'] = str_replace($path, $folder, $file);
        });
        $this->smarty->assign('categories', $categories);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir . 'acp_presskit_index.tpl');
    }

    private function _items($category)
    {
        if (isset($_GET['page']) && $_GET['page'])
            $page = (int)$_GET['page'];
        else
            $page = 1;

        $files = $this->db->getAll("SELECT * FROM presskit_files WHERE category_id = ? LIMIT ? OFFSET ?", array($category['id'], $this->pageSize, ($page - 1) * $this->pageSize));

        $folder = $this->folder;
        array_walk($files, function(&$item) use ($folder) {
            if (isset($item['ext']) && in_array($item['ext'], array('jpg', 'png', 'gif')))
                $item['thumb'] = "{$folder}{$item['hash']}.{$item['ext']}";
        });

        $this->smarty->assign('category', $category);
        $this->smarty->assign('files', $files);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir . 'acp_presskit_items.tpl');
    }

    private function edit()
    {
        $data = array();
        $id = Utils::parseGet('id');
        $category = Utils::parseGet('category_id');
        if ($id) {
            $data = $item = $this->db->getRow("SELECT * FROM presskit_files WHERE id = ? AND category_id = ?", array($id, $category));
        } elseif ($category && !empty($_FILES)) {
            $data['category_id'] = $category;
        } else {
            return;
        }

        $category = $this->db->getRow("SELECT * FROM presskit_categories WHERE id = ?", array($data['category_id']));
        $exts = array_map('trim', explode('|', $category['exts']));

        if (!empty($_POST['title'])) {
            $data['title'] = $_POST['title'];
            $success = $this->upload($data, $exts);
            if ($success) {
                if (isset($item) && $item)
                    $this->db->update('presskit_files', $data, "id = {$item['id']}");
                else
                    $data['id'] = $this->db->insert('presskit_files', $data);
            }
        }
        if (isset($data['ext']) && in_array($data['ext'], array('jpg', 'png', 'gif')))
            $data['thumb'] = "{$this->folder}{$data['hash']}.{$data['ext']}";
        echo json_encode($data);
    }

    private function upload(array &$data, array $exts = array())
    {
        $result = true;
        if (isset($_FILES['file']) && in_array($ext = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1)), $exts)) {
            if (!empty($data['hash'])) {
                foreach (glob("{$this->path}{$data['hash']}*") as $oldFile) {
                    unlink($oldFile);
                }
            }
            $data['ext'] = $ext;
            $data['hash'] = md5(time());
            $path = $this->path;
            if (!file_exists($path)) {
                Tools::create_directory($path, 0777);
            }
            $file = "{$path}{$data['hash']}.{$data['ext']}";

            $content_range = preg_split('/[^0-9]+/', isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : '');
            $append_file = isset($content_range[1]) && $content_range[1] > 0;
            $uploaded_file = $_FILES['file']['tmp_name'];
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                    $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
                } else {
                    $result = move_uploaded_file($uploaded_file, $file) && !isset($content_range[3]);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
                $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
            }
            if ($result) {
                $data['size'] = filesize($file);
                if (in_array($data['ext'], array('jpg', 'png', 'gif'))) {
                    $Img = Image::CreateFromFile($file);
                    foreach ($this->img_sizes as $w => $prfx) {
                        $new_file = "{$path}{$data['hash']}{$prfx}.{$data['ext']}";
                        $Img->resize($w);
                        $Img->save($new_file, 'jpeg');
                    }
                }
            }
        }
        return $result;
    }

    private function delete()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM presskit_files WHERE id = ?", array($id));
            if ($item) {
                foreach (glob("{$this->path}{$item['hash']}*") as $path) {
                    unlink($path);
                }
                $this->db->delete('presskit_files', "id = $id");
            }
        }
        echo json_encode(array('success' => true));
    }

    private function edit_category()
    {
        $id = Utils::parseGet('id');
        $category = Utils::parseGet('category_id');
        $data = array();
        if ($id) {
            $data = $item = $this->db->getRow("SELECT * FROM presskit_categories WHERE id = ?", array($id));
        } else {
            $data['parent'] = isset($_POST['parent']) ? 1 : 0;
        }

        if (!empty($_POST['title'])) {
            $data['title'] = $_POST['title'];
            $data['exts'] = str_replace(' ', '', $_POST['exts']);
            if ($category)
                $data['category_id'] = $category;

            if (isset($item) && $item)
                $this->db->update('presskit_categories', $data, "id = {$item['id']}");
            else
                $data['id'] = $this->db->insert('presskit_categories', $data);

            if (isset($_FILES['file']['tmp_name'])) {
                $ext = substr(strrchr($_FILES['file']['name'], '.'), 1);
                if (in_array($ext, array('jpg', 'png', 'gif'))) {
                    if (!file_exists($this->path)) {
                        Tools::create_directory($this->path, 0777);
                    }
                    $path = "{$this->path}category_{$data['id']}.jpg";
                    $img = Image::CreateFromFile($_FILES['file']['tmp_name']);
                    $img->save($path, 'jpeg');
                    foreach ($this->img_sizes as $w => $prfx) {
                        $new_file = "{$this->path}category_{$data['id']}{$prfx}.jpg";
//                      $img->resize($w); //todo disable resize thumb category presskit
                        $img->save($new_file, 'jpeg');
                        $data['thumb'] = str_replace($this->path, $this->folder, $new_file);
                    }
                }
            }
            echo json_encode($data);
        }
        return;
    }

    private function delete_category()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            foreach (glob("{$this->path}category_{$id}*") as $file) {
                unlink($file);
            }
            $files = $this->db->getAll("SELECT * FROM presskit_files WHERE category_id = ?", array($id));
            foreach ($files as $file) {
                foreach (glob("{$this->path}{$file['hash']}*") as $path) {
                    unlink($path);
                }
            }
            $this->db->delete('presskit_files', "category_id = $id");
            $this->db->delete('presskit_categories', "id = $id");
        }
        echo json_encode(array('success' => true));
    }
}