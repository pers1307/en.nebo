<div class="padding-block">
    <div class="row-fluid">

        <h3>Пресс-кит</h3>
        <a class="btn btn-success addCategory" style="margin-bottom: 20px;" href="?doc_id={$doc_id}&module&action=edit_category&category_id={$category_id}" data-original-title="" data-original-parent="" data-original-exts="">
            <i class="icon-plus icon-white"></i> Добавить категорию
        </a>

        {if ($category_id)}
            <a class="btn btn-info" style="margin-bottom: 20px;" href="?doc_id={$doc_id}&module" data-original-title="">
                <i class="icon-circle-arrow-left icon-white"></i> Список категорий
            </a>
        {/if}

        <ul class="thumbnails">
            {foreach $categories as $category}
                <li class="span3" data-id="{$category.id}">
                    <div class="thumbnail">
                        <a class="itemsLink" href="?doc_id={$doc_id}&module&action=category&category_id={$category_id}&id={$category.id}">
                            {if ($category.thumb)}
                                <div style="height: 155px; overflow: hidden;"><img src="{$category.thumb}" class="span12" /></div>
                            {else}
                                <div style="height: 155px; overflow: hidden;"><img src="/i/nophoto_news.jpg" class="span12" /></div>
                            {/if}
                        </a>
                        <div class="name span12 overflow-text-white">{$category.title}<span>&nbsp;</span></div>
                        <a data-original-title="{$category.title}" data-original-parent="{$category.parent}" data-original-exts="{$category.exts}" href="?doc_id={$doc_id}&module&action=edit_category&category_id={$category_id}&id={$category.id}" class="btn btn-warning btn-mini editCategory"><i class="icon-pencil icon-white"></i></a>
                        <a href="?doc_id={$doc_id}&module&action=delete_category&category_id={$category_id}&id={$category.id}" class="btn btn-danger btn-mini deleteCategory"><i class="icon-remove icon-white"></i></a>
                    </div>
                </li>
            {/foreach}
        </ul>

        <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Новая категория</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="createCategoryForm" action="?doc_id={$doc_id}&module&action=edit_category&category_id={$category_id}" method="post">
                    <div class="control-group">
                        <label class="control-label" for="title">Название</label>
                        <div class="controls">
                            <input type="text" name="title" id="title" class="input-block-level" placeholder="Текущее название категории">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="title">Изображение</label>
                        <div class="controls">
                            <input type="file" id="fileupload" name="file" />
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <label>
                                <input type="checkbox" name="parent" id="parent" />Родительская категория
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="title">Типы файлов</label>
                        <div class="controls">
                            <input type="text" name="exts" id="exts" class="input-block-level" placeholder="Типы через знак |">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary uploadFile">Сохранить изменения</button>
            </div>
        </div>

    </div>
</div>

<script id="categoryTemplate" type="text/html">
    <li class="span3">
        <div class="thumbnail">
            <a class="itemsLink" href=""></a>
            <div class="name span12 overflow-text-white"><span>&nbsp;</span></div>
            <a data-original-title="" href="" class="btn btn-warning btn-mini editCategory"><i class="icon-pencil icon-white"></i></a>
            <a href="" class="btn btn-danger btn-mini deleteCategory"><i class="icon-remove icon-white"></i></a>
        </div>
    </li>
</script>

<script type="application/javascript">
    $(function() {
        var success = function (item) {
            var template = $('[data-id="'+item.id+'"]');
            if (!template.length) {
                template = $($('#categoryTemplate').html())
                $('.thumbnails').append(template);
            }
            template.data('id', item.id);
            $('.itemsLink', template)
                    .attr('href', '?doc_id={$doc_id}&module&action=category&id='+item.id)
                    .html(
                            item.thumb ? '<div style="height: 110px; overflow: hidden;"><img src="'+item.thumb+'?'+Math.random()+'" class="span12"></div>'
                                    : '<img src="/i/nophoto_news.jpg" class="span12">'
                    );
            $('div.name', template).text(item.title);
            $('.editCategory', template)
                    .data('original-title', item.title)
                    .data('original-parent', item.parent)
                    .data('original-exts', item.exts)
                    .attr('href', '?doc_id={$doc_id}&module&action=edit_category&category_id={$category_id}&id='+item.id);
            $('.deleteCategory', template).attr('href', '?doc_id={$doc_id}&module&action=delete_category&category_id={$category_id}&id='+item.id);
            $('#editModal').modal('hide');
        }

        $('.uploadFile').click(function() {
            var form = $('#createCategoryForm');
            $.post(form.attr('action'), form.serialize(), success, 'json');
        });

        $('#fileupload').fileupload({
            autoUpload: false,
            dataType: 'json',
            maxChunkSize: 10485760, // 10mb
            add: function (e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(\.|\/)(gif|jpg|png)$/i;
                if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['name'])) {
                    uploadErrors.push('Неправильный тип файла');
                }
                if(uploadErrors.length > 0) {
                    alert(uploadErrors.join("\n"));
                }

                $('.uploadFile').off().click(function () {
                    data.submit();
                });
            },
            done: function (e, data) {
                if (data.result) {
                    success(data.result);
                }
            }
        });

        $(document)
                .on('click', '.editCategory, .addCategory', function(e) {
                    e.preventDefault();
                    $('#title', '#editModal').val($(this).data('original-title'));
                    $('#exts', '#editModal').val($(this).data('original-exts'));
                    $('#parent', '#editModal').attr('checked', !!$(this).data('original-parent')).attr('disabled', $(this).hasClass('editCategory'));
                    $('#createCategoryForm', '#editModal').attr('action', $(this).attr('href'));
                    $('#editModal').modal('show');
                })
                .on('click', '.deleteCategory', function(e) {
                    e.preventDefault();
                    var self = $(this);
                    $.get(self.attr('href'), function() {
                        self.closest('li').remove();
                    });
                });
    });
</script>