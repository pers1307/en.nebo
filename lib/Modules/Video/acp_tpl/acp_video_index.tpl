<div class="padding-block">
    <div class="row-fluid">
        <h3>Список видео</h3>
        <a class="btn btn-success" style="margin-bottom: 20px;" href="#" data-toggle="modal" data-target="#editModal">
            <i class="icon-plus icon-white"></i> Загрузить видео
        </a>

        <ul class="thumbnails">
            {foreach $videos as $video}
                <li class="span3"{if $video@iteration is div by 5} style="margin-left: 0;"{/if}>
                    <div class="thumbnail">
                        <a href="?doc_id={$doc_id}&module&action=edit&id={$video.id}">
                            {if ($video.thumb)}
                                <div style="height: 110px; overflow: hidden;"><img src="{$video.thumb}" class="span12"></div>
                            {else}
                            <div style="height: 110px; overflow: hidden;"><img src="/i/nophoto_news.jpg" class="span12"></div>
                            {/if}
                        </a>
                        <div class="name span12 overflow-text-white" data-original-title="{$video.title}">{$video.title}<span>&nbsp;</span></div>
                        <a href="?doc_id={$doc_id}&module&action=edit&id={$video.id}" class="btn btn-warning btn-mini"><i class="icon-pencil icon-white"></i></a>
                        <button type="button" class="btn btn-danger btn-mini" onclick="deleteVideo(this, '{$video.id}');"><i class="icon-remove icon-white"></i></button>
                    </div>
                </li>
            {/foreach}
        </ul>

        {assign var=vlink value='?doc_id='|cat:$doc_id|cat:{'&module&'}}
        {$paginator->page_links($vlink)}

        <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Новое видео</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="createVideoForm" action="?doc_id={$doc_id}&module&action=edit" method="post">
                    Название:
                    <input type="text" name="title" id="title" class="input-block-level" placeholder="Текущее название видео">
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary" onclick="$('#createVideoForm').submit();">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(function() {
        $('#createVideoForm').on('submit', function(e) {
            if (!$('[name="title"]').val()) {
                alert('Нужно заполнить название видео');
                e.preventDefault();
            }
        });
    });

    function deleteVideo(element, id) {
        if (confirm('Вы действительно хотите удалить видео?'))
            $.get('?doc_id={$doc_id}&module&action=delete&id='+id, function(data) {
                if (data.success) {
                    $(element).closest('li').remove();
                }
            }, 'json');
    }
</script>