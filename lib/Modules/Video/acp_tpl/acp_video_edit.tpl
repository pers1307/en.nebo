<div class="padding-block">
    <h3>{if ($item.id)}Редактирование{else}Добавление{/if} видео</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module" style="margin-bottom: 20px;">
        <i class="icon-circle-arrow-left icon-white"></i>
        Вернуться к списку
    </a>

    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <div class="control-group">
            <label class="control-label" for="title">Название</label>
            <div class="controls">
                <input type="text" id="title" name="title" placeholder="Название" value="{$item.title|htmlspecialchars}" class="span5">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="title">Тип</label>
            <div class="controls">
                <select name="type" id="type">
                    <option>- выбрать -</option>
                    <option value="news" {if ($item.type == 'news')}selected{/if}>Новостные материалы</option>
                    <option value="presentations" {if ($item.type == 'presentations')}selected{/if}>Презентационное видео</option>
                </select>
            </div>
        </div>
        {if ($item.video.mp4)}
            <div class="control-group">
                <label class="control-label" for="title"></label>
                <div class="controls">
                    <video width="640" height="360" poster="{$item.thumb}" controls="controls" preload="none">
                        <source type="video/mp4" src="{$item.video.mp4}" />
                        <object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf">
                            <param name="movie" value="/js/me/flashmediaelement.swf" />
                            <param name="flashvars" value="controls=true&file={$item.video.mp4}" />
                            <!-- Image as a last resort -->
                            <img src="{$item.thumb}" width="640" height="360" title="No video playback capabilities" />
                        </object>
                    </video>
                </div>
            </div>
        {/if}

        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <span class="help">mp4, flv. Рекомендуемый размер файла не более 50 Мб</span>
                <div class="ajax-fileuploader-form">
                    <span class="btn btn-success btn-mini fileinput-button">
                        <i class="icon-plus icon-white"></i>
                        <span>Загрузить видео</span>
                        <input id="fileupload" type="file" name="video" />
                    </span>

                    <div id="progress" class="progress">
                        <div class="bar"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="title">Время превью</label>
            <div class="controls">
                <input id="maskedinput" type="text" name="time" value="00:00:00" class="span1" />
                <span class="help-inline">Укажите время кадра для превью изображения (час/минута/секунда)</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="create_date">Дата создания</label>
            <div class="controls">
                <input type="text" name="create_date" id="create_date" value="{$item.create_date|date_format:'%d.%m.%Y, %H:%M'}" autocomplete="off" disabled />
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить изменения</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/js/me/mediaelement-and-player.min.js"></script>
<script type="application/javascript" src="/_bo/js/jquery.maskedinput.min.js"></script>
<link rel="stylesheet" href="/js/me/mediaelementplayer.css">
<script type="application/javascript">


    var initPlayer = function () {
        $('video').mediaelementplayer();
    };

    $(function(){
        $('#fileupload').fileupload({
            url: '?doc_id={$doc_id}&module&action=upload&id={$item.id}',
            dataType: 'json',
            maxChunkSize: 10485760, // 10mb
            done: function (e, data) {

                if (data.result) {
                    if(data.result.success)
                        console.log(data.result);
                        initPlayer();
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css('width',progress + '%');
            }
        });

        $.mask.definitions['h'] = '[0-2]';
        $.mask.definitions['m'] = '[0-5]';
        $('#maskedinput').mask('h9:m9:m9');

        $('form').submit(function(e) {
            if (!$('#title').val()) {
                alert('Заполните название');
                e.preventDefault();
            }
            if (!$('#type').val()) {
                alert('Заполните тип');
                e.preventDefault();
            }
        });


        initPlayer();
    });
</script>
