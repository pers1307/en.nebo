<?

include_once R_core . 'Paginator.php';

class ACP_Video extends ACP_Module
{

    protected $name = __CLASS__;

    private $folder = 'videos/';
    private $path;
    private $pageSize = 10;

    //private $img_size = array('img'=>array(400=>'', 162=>'_s'));

    function __construct()
    {
        parent::__construct();
        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;
    }

    function rewrite()
    {
        switch ($this->action) {

            case 'upload':
                $this->upload();
                return false;
                break;

            case 'delete':
                $this->delete();
                return false;
                break;

            case 'edit':
                $this->edit();
                break;

            default:
                $this->index();
                break;

        }
        return true;
    }

    private function index()
    {
        if (isset($_GET['page']) && $_GET['page'])
            $page = (int)$_GET['page'];
        else
            $page = 1;

        $videos = $this->db->getAll("SELECT * FROM videos LIMIT ? OFFSET ?", array($this->pageSize, ($page - 1) * $this->pageSize));
        $videosCount = $this->db->getOne("SELECT COUNT(*) FROM videos");
        $paginator = new Paginator($this->pageSize, 'page');
        $paginator->set_total($videosCount);

        $that = $this;
        array_walk($videos, function (&$item) use ($that) {
            $item = $that->getFiles($item);
        });

        $this->smarty->assign('paginator', $paginator);
        $this->smarty->assign('videos', $videos);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir . 'acp_video_index.tpl');
    }

    private function edit()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM videos WHERE id = ?", array($id));
            $item = $this->getFiles($item);
            $this->smarty->assign('item', $item);
            if (isset($_POST['time'])) {
                $file = "{$this->path}{$item['id']}/video_{$item['id']}";
                $this->create_thumb("{$file}.mp4", "{$file}.jpg", false);
            }
        }
        if (!empty($_POST['title'])) {
            $data = array('title' => $_POST['title']);
            if (!empty($_POST['type'])) $data['type'] = $_POST['type'];

            if (isset($item) && $item) {
                $this->db->update('videos', $data, "id = {$item['id']}");
            } else {
                $data['create_date'] = time();
                $id = $this->db->insert('videos', $data);
            }
            header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
            return;
        }

        $this->controller->body = $this->smarty->fetch($this->tpl_dir . 'acp_video_edit.tpl');
    }

    private function upload()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM videos WHERE id = ?", array($id));
            if (!empty($_FILES)) {
                $success = $this->upload_video('video', $id);
                if ($success && !$item['enabled']) {
                    $this->db->update('videos', array('enabled' => 1), "id = {$item['id']}");
                }
                echo json_encode(array('success' => $success));
            }
        }
    }

    private function upload_video($name, $video_id)
    {
        $result = false;
        $type = substr($_FILES[$name]['type'], -3);
        if (isset($_FILES[$name]) && in_array($type, array('flv', 'mp4'))) {
            $path = $this->path . $video_id . '/';
            if (!file_exists($path)) {
                Tools::create_directory($path, 0777);
            }
            $file = "{$path}video_{$video_id}.{$type}";

            $content_range = preg_split('/[^0-9]+/', isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : '');
            $append_file = isset($content_range[1]) && $content_range[1] > 0;
            $uploaded_file = $_FILES[$name]['tmp_name'];
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                    $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
                } else {
                    $result = move_uploaded_file($uploaded_file, $file) && !isset($content_range[3]);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
                $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
            }
            if ($result && $type == 'mp4') {
                $this->create_thumb($file, str_replace('mp4', 'jpg', $file));
            }
        }
        return $result;
    }

    private function create_thumb($video, $image, $rand = true)
    {
        $ffmpeg = FFMPEG;

        if (isset($_POST['time']) && preg_match('/\d{2}:\d{2}:\d{2}/', $_POST['time']) && $_POST['time'] != '00:00:00') {
            $time = $_POST['time'];
        } elseif ($rand) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} 2>&1");
            $duration = ob_get_clean();
            preg_match('/Duration: (.*?),/', $duration, $matches);
            $duration = $matches[1];
            $duration_array = explode(':', $duration);
            $duration = $duration_array[0] * 3600 + $duration_array[1] * 60 + $duration_array[2];
            $time = gmdate('H:i:s.u', mt_rand(1, $duration - 1));
        }


        if (isset($time) && file_exists($video)) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} -ss {$time} -f image2 -vframes 1 {$image}");
            ob_end_clean();
        }
    }

    private function delete()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            $path = "{$this->path}{$id}";
            $files = glob($path . '/video*');
            foreach ($files as $file) {
                unlink($file);
            }
            if (is_dir($path))
                rmdir($path);
            $this->db->delete('videos', "id = $id");
        }
        echo json_encode(array('success' => true));
    }

    public function getFiles($item)
    {
        $thumb = $this->path . $item['id'] . "/video_{$item['id']}.jpg";
        if (file_exists($thumb))
            $item['thumb'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $thumb) . '?' . filesize($thumb);

        $mp4 = $this->path . $item['id'] . "/video_{$item['id']}.mp4";
        if (file_exists($mp4))
            $item['video']['mp4'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $mp4);

        $flv = $this->path . $item['id'] . "/video_{$item['id']}.flv";
        if (file_exists($flv))
            $item['video']['flv'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $flv);

        return $item;
    }
}