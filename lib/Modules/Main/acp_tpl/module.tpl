{include file="tabs.tpl"}
<div class="dinamic-edit">
 <div id="de_module" class="dinamic-edit-item">
  {$body}
 </div>
</div>
<script type="text/javascript">
var doc_id = '{$doc_id}';
{literal}
$(document).ready(function() {
	$('.dinamic-edit-item').hide()
	$('#de_module').show()
	$('.page-tabs a').removeClass('act')
	$('.page-tabs a#pt_module').addClass('act')
})
</script>
{/literal}