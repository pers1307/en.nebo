<div class="padding-block">
    {if !$fl_folder.id}
        <h3>Галереи</h3>
        <button class="btn btn-success" onclick="galleries_create_folder()"><i class="icon-picture icon-white"></i> Создать галерею</button>
    {else}
        <h3>Галерея: {$fl_folder.title}</h3>
        <a href="/_bo/?action=galleries{if ($fl_folder.parent_id)}&fid={$fl_folder.parent_id}{/if}" class="btn btn-info"><i class="icon-circle-arrow-left icon-white"></i> В список галерей</a>
        <br /><br />
    {/if}
	{if ($fl_folder.id)}
	    <form method="post" enctype="multipart/form-data" action="?action=galleries" class="form-inline">
	        <input type="hidden" name="act" value="upload_files" />
	        <input type="hidden" name="folder_id" id="folder_id" value="{$fl_folder.id|intval}" />
	        <div class="acp_input_files well">
	            <div id="input_files">
	                <div class="input_file">
	                    <input type="file" name="file[]" placeholder="Выберите файл" />
	                    <div class="input-append">
	                        <input type="text" class="text" name="file_title[]" placeholder="Укажите название файла" autocomplete="off" />
	                        <button type="button" onclick="add_acp_files_input()" class="btn"><i class="icon-plus-sign"></i></button>
	                    </div>
	                </div>
	            </div>
                <button type="submit" class="btn btn-success"><i class="icon-hdd icon-white"></i> Загрузить выбранные файлы</button>
	        </div>
	    </form>
	{/if}

</div>

{if ($fl_galleries)}
    {if !$fl_folder.id}
        <table class="table !data_table acp_files">
            <tr>
                <th>Название</th>
                <th style="width: 105px;">Дата</th>
                <th>&nbsp;</th>
            </tr>
            {foreach from=$fl_galleries item=file}
                <tr id="tr_{$file.id}">
                    <td class="td-title">
                        {if ($file.type == 'folder')}
                            <i class="icon-folder-open"></i>
                            <a href="/_bo/?action=galleries&fid={$file.id}" class="title">{$file.title}</a>
                        {elseif ($file.type == 'file')}
                            <i class="icon-file"></i>
                            <span class="title">{$file.title}</span>.{$file.ext}
                        {/if}
                    </td>
                    <td class="center">{$file.create_date|date_format:'%d.%m.%Y  %H:%M'}</td>
                    <td class="center">
                        <button type="button" onclick="acp_gallery_rename({$file.id})" class="btn btn-warning btn-mini" /><i class="icon-tag icon-white"></i></button>
                        {if ($file.type == 'folder')}
                            {if (!$file.children)}
                                <button type="button" onclick="acp_gallery_delete({$file.id}, '{$file.title}')" class="btn btn-danger btn-mini" /><i class="icon-remove icon-white"></i></button>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </table>
    {else}
        <div class="container-fluid">
            <div class="row-fluid">
                <ul class="thumbnails" id="sortable_thumb">
                    {foreach from=$fl_galleries item=file name=thumb}
                        <li class="span3" id="tr_{$file.id}" data-id="{$file.id}">
                            <div class="thumbnail">
                                <img src="{if !empty($file.thumb)}{$file.thumb}{else}{$file.link}{/if}" data-src="{if !empty($file.thumb)}{$file.thumb}{else}{$file.link}{/if}" class="span12" />
                                <div class="name span12 overflow-text-white" title="{$file.title|htmlspecialchars}">{$file.title}<span>&nbsp;</span></div>
                                <button type="button" class="btn btn-warning btn-mini" data-toggle="modal" data-target="#renameModal" onclick="fileId='{$file.id}'; $('#renameModal #title').val($('div#original-title-{$file.id}').text()); $('#renameModal #description').val($('div#original-description-{$file.id}').text());" /><i class="icon-pencil icon-white"></i></button>
                                <button type="button" class="btn btn-info btn-mini" onclick="rotateImage('{$file.id}');" /><i class="icon-repeat icon-white"></i></button>
                                {if $thumbCropManual}
                                    <button type="button" class="btn btn-info btn-mini" data-toggle="modal" data-target="#cropModal" onclick="fileId='{$file.id}'; fileLink='{$file.link}'; prepareCropModal();" /><i class="icon-th icon-white"></i></button>
                                {/if}
                                <button type="button" class="btn btn-danger btn-mini" data-toggle="modal" data-target="#delModal" onclick="fileId='{$file.id}'; filePath='{$file.title}.{$file.ext}';" /><i class="icon-remove icon-white"></i></button>
                                <span class="btn btn-mini sort_move"><i class="icon-move"></i></span>

                                <div id="original-description-{$file.id}" class="hide">{$file.description|htmlspecialchars}</div>
                                <div id="original-title-{$file.id}" class="hide">{$file.title|htmlspecialchars}</div>
                            </div>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>

        {* ---- Переименовать ---- *}
        <div id="renameModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Укажите название и описание</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline">
	                Название:
                    <input type="text" id="title" class="input-block-level" placeholder="Текущее название изображения">

                    Описание:
                    <input type="text" id="description" class="input-block-level" placeholder="Текущее описание изображения">
                    <span class="fl_callback"></span>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary" onclick="renameFromModal();">Сохранить изменения</button>
            </div>
        </div>

        {* ---- Удалить ---- *}
        <div id="delModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-body">
                <h4 class="text-warning">Вы действительно хотите удалить изображение?</h4>
            </div>
            <div class="modal-footer text-center">
                <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-ban-circle"></i> Нет</button>
                <button class="btn btn-danger" onclick="deleteFromModal();"><i class="icon-trash icon-white"></i> Да, удалить</button>
            </div>
        </div>

        {if $thumbCropManual}
            {* ---- Обрезка ---- *}
            <div id="cropModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelCrop" aria-hidden="true" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabelCrop">Создание миниатюры</h3>
                </div>
                <div class="modal-body text-center"></div>
                <div class="modal-footer text-center">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" onclick="saveCropFromModal();">Сохранить изменения</button>
                </div>
            </div>
        {/if}
    {/if}
{else}
    <div class="padding-block">Загруженные файлы отсутствуют</div>
{/if}

{if $thumbCropManual}
    <script type="text/javascript" src="/_bo/js/jquery.jcrop.js"></script>
    <link type="text/css" rel="stylesheet" href="/_bo/s/jquery.jcrop.min.css"/>
{/if}

<script type="text/javascript">
    $('.acp_files textarea').click(function() {
        $(this).select();
    })

    var fileId = '';
    var filePath = '';
    var fileLink = '';
    var jc;

    function deleteFromModal() {
        acp_gallery_delete(fileId, filePath);
        $('#delModal').modal('hide');
    }

    function rotateImage(fid) {
        $.post('/_bo/?action=galleries', { act:'rotate_file', id: fid }, function(data) {
            if (data == 1) {
                var $im = $('#tr_'+fid+' img');
                var dt = new Date();
                $im.attr('src', $im.attr('data-src')+'?'+dt.getTime());
            } else {
                $('.fl_callback').html(data);
            }
        });
    }

    function renameFromModal() {
        $.post('/_bo/?action=galleries', { act:'rename_file', id:fileId, title:$('#renameModal #title').val(), description: $('#renameModal #description').val()}, function(data) {
            if (data == 1) {
                $('#tr_'+fileId+' div.name').text($('#renameModal #title').val());
                $('div#original-title-'+fileId).text($('#renameModal #title').val());
                $('div#original-description-'+fileId).text($('#renameModal #description').val());
                $('#renameModal').modal('hide');
            } else {
                $('.fl_callback').html(data);
            }
        });
    }

    {if $thumbCropManual}
        function prepareCropModal() {
            var cropModalHeight = 400;
            var src = fileLink+'?'+Math.random()*1000;
            var newImg = new Image();
            newImg.src = src;

            newImg.onload = function() {
                var $img = $(newImg);
                var origW = newImg.width;
                var origH = newImg.height;
                $img.attr({
                    'width': Math.round(newImg.width*cropModalHeight/newImg.height),
                    'height': cropModalHeight
                });

                $('#cropModal div.modal-body').empty().append($img);
                $('#cropModal div.modal-body img').Jcrop({
                    'aspectRatio': Math.round( {$thumbSizeX}/{$thumbSizeX}, 2 ),
                    'minSize': [{$thumbSizeX}, {$thumbSizeX}],
                    'trueSize': [origW, origH]
                }, function() {
                    jc = this;
                });
            }
        }

        function saveCropFromModal() {
            var select = jc.tellSelect();
            $.post('/_bo/?action=galleries', { act:'crop_file', id:fileId, select: select }, function(data) {
                if (data == 1) {
                    var $im = $('#tr_'+fileId+' img');
                    var dt = new Date();
                    $im.attr('src', $im.attr('data-src')+'?'+dt.getTime());
                    $('#cropModal').modal('hide');
                } else {
                    $('.fl_callback').html(data);
                }
            });
        }
    {/if}

    $("#sortable_thumb").sortable({
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        items: "> [data-id]",
        handle: ".sort_move",
        stop: function() {
            var data = { ids:[] };

            $('#sortable_thumb li[data-id]').each(function(i, el){
                data[i] = $(el).data('id');
            });
            $.post('/_bo/?action=galleries', { act:'sort_thumb', id:{$smarty.get.fid}, ids:data });
        }
    }).disableSelection();
</script>

{*
<td class="center">
<button type="button" onclick="acp_gallery_rename({$file.id})" class="btn btn-warning btn-mini" /><i class="icon-tag icon-white"></i></button>
{if ($file.type == 'file')}
<button type="button" onclick="acp_gallery_delete({$file.id}, '{$file.title}.{$file.ext}')" class="btn btn-danger btn-mini" /><i class="icon-remove icon-white"></i></button>
    {elseif ($file.type == 'folder')}
    {if (!$file.children)}
    <button type="button" onclick="acp_gallery_delete({$file.id}, '{$file.title}')" class="btn btn-danger btn-mini" /><i class="icon-remove icon-white"></i></button>
        {else}
    не удалить
    {/if}
{/if}
</td>
*}