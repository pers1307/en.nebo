<div class="padding-block">
    <h3>Файлы</h3>

    {* UPLOAD FORM *}
    <form method="post" enctype="multipart/form-data" action="?action=filelibrary&act=upload_files" class="form-inline">
        <input type="hidden" name="folder_id" id="folder_id" value="{$fl_folder.id|intval}" />

        <div class="acp_input_files">
            <div id="input_files" class="well">
                <div class="input_file">
                    <input type="file" name="file[]" placeholder="Выберите файл" />
                    <div class="input-append">
                        <input type="text" class="text" name="file_title[]" placeholder="Укажите название файла" autocomplete="off" />
                        <button type="button" onclick="add_acp_files_input()" class="btn"><i class="icon-plus-sign"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-small btn-success"><i class="icon-hdd icon-white"></i> Загрузить файлы</button>
    </form>

{*
<p style="line-height: 18px;">
{if ($fl_folder.id)}
 <strong><img src="/_bo/images/folder_16.png" /> {$fl_folder.title}</strong>
{/if}
 <button class="btn btn-small btn-success" onclick="filelibrary_create_folder()">Создать папку</button>
</p>
<p class="fl_callback"></p>
*}
</div>

{* FOLDER LIST *}
{if ($fl_folder.id)}
    <tr>
        <td colspan="6">
            <a href="/_bo/?action=filelibrary{if ($fl_folder.parent_id)}&fid={$fl_folder.parent_id}{/if}" class="btn btn-info btn-small"><i class="icon-arrow-up icon-white"></i></a>
        </td>
    </tr>
{/if}

{* FILES LIST *}
{if ($fl_files)}
    <table class="table !data_table acp_files">
        <tr>
            <th>Название</th>
            <th>Размер</th>
            <th style="width: 200px;">Ссылка</th>
            <th style="width: 105px;">Дата</th>
            <th>&nbsp;</th>
        </tr>


        {foreach from=$fl_files item=file}
            <tr id="tr_{$file.id}">
                <td class="td-title">
                    {if ($file.type == 'folder')}
                        <i class="icon-folder-open"></i>
                        <a href="/_bo/?action=filelibrary&fid={$file.id}" class="title">{$file.title}</a>
                    {elseif ($file.type == 'file')}
                        <i class="icon-file"></i>
                        <span class="title">{$file.title}</span>.{$file.ext}
                    {/if}
                </td>
                <td>{if ($file.type == 'file')}{$file.size}{/if}</td>
                <td class="td-link">
                    {if ($file.type == 'file')}
                        <input type="hidden" class="link" value="{$file.link}" />
                        <input type="hidden" class="ext" value="{$file.ext}" />
                        <span class="ajx" onclick="acp_show_file_link({$file.id})">Показать ссылку</span>
                        <textarea readonly="readonly"></textarea>
                    {/if}
                </td>
                <td class="center">{$file.create_date|date_format:'%d.%m.%Y  %H:%M'}</td>
                <td class="center">
                    <button type="button" onclick="acp_file_rename({$file.id})" class="btn btn-warning btn-mini" /><i class="icon-tag icon-white"></i></button>
                    {if ($file.type == 'file')}
                        <button type="button" class="btn btn-info btn-mini _openCoverDialog" data-toggle="modal" data-target="#editModal" {if $file.cover == 1}data-cover="/u/{$file.fullpath}{$file.hash}_cover.jpg"{/if} data-rel="{$file.id}" /><i class="icon-picture icon-white"></i></button>
                        <button type="button" onclick="acp_file_delete({$file.id}, '{$file.title}.{$file.ext}')" class="btn btn-danger btn-mini" /><i class="icon-remove icon-white"></i></button>
                    {elseif ($file.type == 'folder')}
                        {if (!$file.children)}
                            <button type="button" onclick="acp_file_delete({$file.id}, '{$file.title}')" class="btn btn-danger btn-mini" /><i class="icon-remove icon-white"></i></button>
                        {else}
                            <img src="images/deln_disabled.gif" title="Папка не пуста" />
                        {/if}
                    {/if}
                </td>
            </tr>
        {/foreach}
    </table>
{else}
    <div class="padding-block">Загруженные файлы отсутствуют</div>
{/if}

{* MODAL EDIT *}

<div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-header">
        <h3 id="myModalLabel">Обложка для файла</h3>
    </div>
    <div class="modal-body">
        {* image *}
        <div class="control-group">

            <div class="ajax-fileuploader-form">
                <span class="btn btn-success btn-mini fileinput-button">
                    <i class="icon-picture icon-white"></i>
                    <span>Загрузить обложку</span>
                    <input id="fileupload" type="file" name="file" rel="" />
                </span>
                <div id="progress" class="progress">
                    <div class="bar"></div>
                </div>
            </div>

            <div id="images" class="ajax-fileuploader-img">
                <ul class="thumbnails" id="img" style="margin-left: 0;">
                    <li>
                        <a class="thumbnail"></a>
                        <span class="btn btn-mini btn-danger _deleteCover" style="margin-top: 5px;"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                    </li>
                </ul>
            </div>
        </div>
        {* image *}
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var $modal = $('#editModal'),
                $thumbnail = $modal.find('.thumbnail'),
                $fileupload = $modal.find('#fileupload'),
                $progressBar = $('.bar', '#progress'),
                $images = $('#images');

        $(document).on('click', '._deleteCover', function () {
            $.post('?action=filelibrary&act=delete_cover',
                    { id: $fileupload.attr('rel') },
                    function() {
                        $images.hide();
            })
        });

        $(document).on('click', '._openCoverDialog', function () {
            var obj = $(this);
            if (obj.data('cover')) {
                $thumbnail.html('<img src="' + obj.data('cover') + '" />');
            }
            $fileupload.attr('rel', obj.data('rel'));
            if ($images.find('li img').length)
                $images.show();
            else
                $images.hide();
        });

        $modal.on('hidden.bs.modal', function () {
            $fileupload.attr('rel', '');
            $thumbnail.html('');
            $progressBar.css('width', '0%');
        });

        $('#fileupload').fileupload({
            url: '?action=filelibrary&act=upload_cover',
            dataType: 'json',
            submit: function (e, data) {
                data.formData = { id: $fileupload.attr('rel') };
            },
            done: function (e, data) {
                if (data.result.img) {
                    $thumbnail.html('<img src="' + data.result.img + '?' + Math.random() + '" />');
                    $images.show();
                }
                if (data.result.success == false) {
                    alert(data.result.error)
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $progressBar.css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('.acp_files textarea').click(function () {
            $(this).select()
        })
    });
</script>