<!DOCTYPE html>
<html class="login-page">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Vivo Group" />
<meta name="robots" content="noindex, nofollow" />
<title>Welcome Vivo Content</title>

 <link href="/_bo/s/bootstrap.css" rel="stylesheet">
 <link href="/_bo/s/bootstrap-responsive.css" rel="stylesheet">
 <link href="/_bo/s/design.css" rel="stylesheet">
	
 <script src="/_bo/js/!jquery-1.7.2.min.js" type="text/javascript"></script>
 <script src="/_bo/js/{$implode_bo_js}" type="text/javascript"></script>
 <script src="/_bo/ckeditor/ckeditor.js" type="text/javascript"></script>
 <script src="/_bo/ckeditor/adapters/jquery.js" type="text/javascript"></script>
 <script type="text/javascript">var doc_id = {if ($doc.id)}{$doc.id}{else}false{/if};</script>
 
</head>
<body onload="document.getElementById('user_field').focus();">
<div class="bg-gradient">
    <div class="container">
        <form method="post" action="" style="margin: 0px;">
            <div class="well">
                <input type="text" name="username" id="user_field" placeholder="Пользователь">
                <input type="password" name="password" placeholder="Пароль">
                <button class="btn btn-small btn-success" type="submit"><i class="icon-user icon-white"></i> Войти</button>
            </div>
            {if !empty($errors)}
                <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>{$errors}</div>
            {/if}
        </form>
    </div>
</div>
</body>
</html>