<div class="padding-block">
    <div class="container-fluid">
        <div class="row-fluid main-modules">
            <span class="span7">
                <p class="lead muted">Установленные модули</p>
                <ul class="nav nav-tabs nav-stacked">
                    {foreach from=$modules_list item=module}
                        <li><a href="?doc_id={$module.id}&module">{$module.name}</a></li>
                    {/foreach}
                </ul>
            </span>
        </div>
    </div>
</div>