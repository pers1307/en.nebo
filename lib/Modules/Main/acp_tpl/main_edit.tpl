<div class="padding-block">
    <h3>Редактирование главной страницы</h3>
</div>

<div class="padding-block">
    <div class="row-fluid">
        <div class="control-group">
            <div class="ajax-fileuploader-form">
                <span class="btn btn-success btn-mini fileinput-button">
                    <i class="icon-picture icon-white"></i>
                    <span>Загрузить изображение в шапку</span>
                    <input id="fileupload" type="file" name="file" multiple />
                </span>
                <div id="progress" class="progress">
                    <div class="bar"></div>
                </div>
            </div>
        </div>
        <ul class="thumbnails headers" id="sortable_thumb">
            {foreach $headers as $header}
                <li class="span3" data-id="{$header.id}">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="/u/header/{$header.hash}.{$header.ext}" class="span12" />
                        </div>
                        <a href="?action=mainpage&act=delete_header&p={$header.id}" class="btn btn-danger btn-mini deleteHeader"><i class="icon-remove icon-white"></i></a>
                        <span class="btn btn-mini sort_move"><i class="icon-move"></i></span>
                    </div>
                </li>
            {/foreach}
        </ul>

    </div>

    <label>Текст на главной</label>
    <textarea name="body" id="body">{$main_text}</textarea>
    <br />
    <span class="btn btn-success saveText" style="margin-left: 0;">
        <i class="icon-ok icon-white"></i>
        <span>Сохранить текст</span>
    </span>
    <br />
    <br />
    <br />

    <p>Баннеры</p>
    <div class="row-fluid">
        <ul class="thumbnails">
            {foreach $banners as $banner}
            <li class="span3">
                <div class="thumbnail">
                    <div class="thumb" style="height: 115px; overflow: hidden;">
                        {if ($banner.thumb)}
                            <img src="{$banner.thumb}" class="span12">
                        {else}
                            <img src="/i/nophoto_news.jpg" class="span12">
                        {/if}
                    </div>
                    <div class="name span12 overflow-text-white">{$banner.title}<span>&nbsp;</span></div>
                    <a href="?action=mainpage&act=edit_banner&id={$banner.id}" class="btn btn-warning btn-mini"><i class="icon-pencil icon-white"></i></a>
                </div>
            </li>
            {/foreach}
        </ul>
    </div>

</div>

<script type="application/javascript">
    CKEDITOR.replace('body', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "250" });

    $(function(){
        $('#fileupload').fileupload({
            url: '?action=mainpage&act=upload_header',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.success) {
                    /*
                    $('.headers').append(
                            '<li class="span3">'+
                            '<div class="thumbnail">'+
                            '<div class="thumb">'+
                            '<img src="'+data.result.link+'" />'+
                            '</div>'+
                            '</div>'+
                            '</li>');
                    */
                    location.reload();
                } else {
                    alert(data.result.error);
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css('width',progress + '%');
            }
        });

        $(document)
            .on('click', '.saveText', function() {
                $.post('?action=mainpage&act=update_text', { text: CKEDITOR.instances.body.getData()}, function() {
                    alert("Сохранено");
                }, 'json');
            })

            .on('click', '.deleteHeader', function(e) {
                e.preventDefault();
                var self = $(this);
                $.get(this.href, function(data) {
                    if (data.success)
                        self.closest('li').remove();
                }, 'json')
            });


        $("#sortable_thumb").sortable({
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            items: "> [data-id]",
            handle: ".sort_move",
            stop: function() {
                var data = { ids:[] };
                $('#sortable_thumb li[data-id]').each(function(i, el){
                    data.ids[i] = $(el).data('id');
                });
                $.post('?action=mainpage&act=sort_image_header', data);
            }
        }).disableSelection();
    });
</script>