<table class="acp_files widget" id="filelibrary_widget" style="width: 600px;">
    {if ($fl_folder.id)}
        {*
        <tr>
            <td colspan="5" style="padding-bottom: 3px;">
                <img src="/_bo/images/folder_16.png" />
                <span title="{$fl_folder.title}">{$fl_folder.short_title}</span>
                &nbsp;
                <img src="images/cat_tree_up.gif" />
                <span class="ajx" onclick="CKEDITOR.tools.callFunction(ck_get_filelink, {$fl_folder.parent_id}, '{$fl_ck_name}')">На уровень выше</span>
            </td>
        </tr>
        *}
    {/if}

    {if ($fl_files)}
        {foreach from=$fl_files item=file}
            <tr>
                <td class="td-checkbox">
                    {if ($file.type == 'folder')}
                        [{$file.children}]
                    {elseif ($file.type == 'file')}
                        <input type="checkbox" class="checkbox" id="fl_check_{$file.id}" onchange="ck_add_link()" />
                    {/if}
                </td>
                {if ($file.type == 'folder')}
                    <td class="td-title" colspan="3">
                        <img src="/_bo/i/filetypes/{$file.ext}.gif" width="21" height="21" />
                        <span class="ajx" title="{$file.title}" onclick="CKEDITOR.tools.callFunction(ck_get_filelink, {$file.id|intval}, '{$fl_ck_name}')">{$file.title_short}</span>
                    </td>
                {elseif ($file.type == 'file')}
                    <td class="td-icon">
                        <img src="/_bo/i/filetypes/{$file.ext}.gif" width="21" height="24" />
                    </td>
                    <td class="td-title">
                        <label for="fl_check_{$file.id}" title="{$file.title}.{$file.ext}">{$file.title_short}.{$file.ext}</label>
                        <input type="hidden" id="fl_link_{$file.id}" value="{$file.full_link}" />
                        <input type="hidden" id="fl_title_{$file.id}" value="{$file.title}" />
                        <input type="hidden" id="fl_ext_{$file.id}" value="{$file.ext}" />
                        <input type="hidden" id="fl_cover_{$file.id}" value="{if $file.cover != 0}/u/{$file.fullpath}{$file.hash}_cover.jpg{else}/i/no_cover_file.jpg{/if}" />
                    </td>
                    <td class="td-insert">
                        <span class="ajx" onclick="ck_insert_link({$file.id}, '{$fl_ck_name}')">вставить</span>
                    </td>
                    <td class="td-size">{$file.size}</td>
                {/if}
                <td class="td-date">{$file.create_date|date_format:'%d.%m.%Y  %H:%M'}</td>
            </tr>
        {/foreach}
    {/if}
</table>