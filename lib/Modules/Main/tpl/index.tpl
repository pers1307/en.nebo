{include file="header.tpl"}

<div class="wrapper-content">
    <div class="content">
        <!--
        <div class="arrows">
            <div class="left"></div>
            <div class="right"></div>
        </div>
        -->
        <div class="airports flexslider">
            <ul class="slides">
                {foreach $air_info as $airports}
                <li class="item">
                    <a href="/airports/{$airports.id}">
                        <div class="title">{$airports.title}</div>
                        <div class="img">
                            {if ($airports.main)}
                                <img src="{$airports.main}" />
                            {else}
                                <img src="/i/air_no.jpg" />
                            {/if}
                            <div class="gradient"></div>
                        </div>
                        <p class="hide">{$airports.description_main}</p>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
        {$vars.index_text}
    </div>
</div>

<div class="banners-block">
    {foreach $banners as $banner}
        <a href="{$banner.link}" class="banner">
            <table>
                <tr>
                    <td class="title">{$banner.title}</td>
                </tr>
                <tr>
                    <td class="img">
                        <div class="label {if $banner.important}hot{/if}">{$banner.text}</div>
                        <div class="mask"><img src="{$banner.thumb}" /></div>
                    </td>
                </tr>
            </table>
        </a>
    {/foreach}
</div>

<div class="sub-footer"></div>
</div>

{include file="footer.tpl"}
