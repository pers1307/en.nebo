<?

include_once "SearchEngine.php";

class Search extends Module {

	protected $name = __CLASS__;

	private $limit = 10;

	private $folder = 'news/';
	private $path;
    private $link;


	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
	}

	function getContent() {
		$this->smarty->assign('news_link', $this->link);

        $this->page->doc['head'] = '';

		$this->index();

		return true;
	}

	function index() {
        $query = isset($_GET['q']) ? $_GET['q'] : '';
        $results = false;
        if ($query) {
            $engine = new SearchEngine();
            $results = $engine->search($query);
        }

        $this->smarty->assign('results', $results);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'index.tpl');
    }

}