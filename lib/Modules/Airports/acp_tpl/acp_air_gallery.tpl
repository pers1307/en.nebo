<div class="padding-block">
    <h3>Аэропорт &laquo;{$item_info.title}&raquo;</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку аэропортов
    </a>
</div>
<div class="padding-block">
    <ul class="nav nav-tabs" style="margin-bottom: 0;" id="sortable">
        <li><a href="?doc_id={$doc_id}&module&action=airport&id={$smarty.get.id}">Об аэропорте</a></li>
        {foreach $item_sections as $link}
            <li data-id="{$link.id}"><a href="?doc_id={$doc_id}&module&action=edit&airport={$link.id_airport}&page={$link.id}"{if $link.visible == 0} class="muted"{/if}>{$link.name}</a></li>
        {/foreach}
        <li class="active"><a href="#">Фотогалерея</a></li>
        <li><a href="?doc_id={$doc_id}&module&action=news&id={$smarty.get.id}">Новости</a></li>
    </ul>
    <div class="padding-block" style="background: #fff; border: 1px solid #ddd; border-top: none;">
        <div class="row-fluid">

            <button type="button" class="btn btn-success createAlbum" style="margin-bottom: 20px;">
                <i class="icon-plus icon-white"></i>Создать альбом
            </button>

            <ul class="thumbnails sort" id="sortable_thumb">
                {foreach $albums as $album}
                    <li class="span3" data-id="{$album.id}"{* if $album@iteration is div by 5}style="margin-left: 0;"{/if *}>
                        <div class="thumbnail">
                            <a href="?doc_id={$doc_id}&module&action=edit_album&id={$album.id}">
                                {if !empty($album.hash)}
                                    <div style="height: 110px; overflow: hidden;"><img src="/u/album/{$album.id}/{$album.hash}.{$album.ext}" class="span12"></div>
                                {else}
                                <div style="height: 110px; overflow: hidden;"><img src="/i/nophoto_news.jpg" class="span12"></div>
                                {/if}
                            </a>
                            <div class="name span12 overflow-text-white" data-original-title="{$album.title}">{$album.title}<span>&nbsp;</span></div>
                            <button type="button" class="btn btn-warning btn-mini editAlbum"><i class="icon-pencil icon-white"></i></button>
                            <button type="button" class="btn btn-danger btn-mini" onclick="deleteAlbum('{$album.id}');"><i class="icon-remove icon-white"></i></button>
                            <span class="btn btn-mini sort_move"><i class="icon-move"></i></span>
                            <div class="original-title" class="hide" style="display: none">{$album.title}</div>
                        </div>
                    </li>
                {/foreach}
            </ul>

        </div>
    </div>

    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Новый альбом</h3>
        </div>
        <div class="modal-body">
            <form class="form-inline" id="editAlbumForm" action="?doc_id={$doc_id}&module&action=update_album&id={$smarty.get.id}" method="post">
                Название:
                <input type="text" name="title" id="title" class="input-block-level" placeholder="Текущее название альбома">
                <input type="hidden" name="album" id="album" value=""/>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
            <button class="btn btn-primary" onclick="$('#editAlbumForm').submit();">Сохранить изменения</button>
        </div>
    </div>
    <script type="application/javascript">
        $(function() {
            $('#editAlbumForm').on('submit', function(e) {
                if (!$('[name="title"]').val()) {
                    alert('Нужно заполнить название альбома');
                    e.preventDefault();
                }
            });

            $(document).on('click', '.editAlbum, .createAlbum', function() {
                var modal = $('#editModal');
                if ($(this).hasClass('editAlbum')) {
                    var parent = $(this).closest('li');
                    $('[name="title"]', modal).val(parent.find('.original-title').text());
                    $('[name="album"]', modal).val(parent.data('id'));
                }
                modal.modal();
            });
        });

        function deleteAlbum(id) {
            if (confirm('Вы действительно хотите удалить альбом?'))
                $.post('?doc_id={$doc_id}&module&action=delete_album', { id:id }, function() {
                    $('[data-id="'+id+'"]').remove();
                });
        }

        $("#sortable").sortable({
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            axis: "x",
            items: "> [data-id]",
            stop: function() {
                var data = { ids:[] };
                $('#sortable li[data-id]').each(function(i, el){
                    data.ids[i] = $(el).data('id');
                });
                $.post('?doc_id={$doc_id}&module&id={$smarty.get.id}&action=sort_sections', data);
            }
        }).disableSelection();

        $("#sortable_thumb").sortable({
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            items: "> [data-id]",
            handle: ".sort_move",
            stop: function() {
                var data = { ids:[] };
                $('#sortable_thumb li[data-id]').each(function(i, el){
                    data.ids[i] = $(el).data('id');
                });
                $.post('?doc_id={$doc_id}&module&id={$smarty.get.id}&action=sort_thumb_albums', data);
                console.log(data);
            }
        }).disableSelection();
    </script>

</div>