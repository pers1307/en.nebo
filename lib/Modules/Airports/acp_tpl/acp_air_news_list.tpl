<div class="padding-block">
<h3>Список новостей</h3>
<a class="btn btn-success" href="?doc_id={$doc_id}&module&action=edit_news&airport={$smarty.get.id}">
	<i class="icon-plus icon-white"></i>
	Добавить новость
</a>
</div>

<div class="padding-block">
	<ul class="nav nav-tabs" style="margin-bottom: 0;" id="sortable">
		<li><a href="?doc_id={$doc_id}&module&action=airport&id={$smarty.get.id}">Об аэропорте</a></li>
		{foreach $item_sections as $link}
			<li data-id="{$link.id}"><a href="?doc_id={$doc_id}&module&action=edit&airport={$link.id_airport}&page={$link.id}"{if $link.visible == 0} class="muted"{/if}>{$link.name}</a></li>
		{/foreach}
		<li><a href="?doc_id={$doc_id}&module&action=gallery&id={$smarty.get.id}">Фотогалерея</a></li>
		<li class="active"><a href="#">Новости</a></li>
	</ul>
	<div class="padding-block" style="background: #fff; border: 1px solid #ddd; border-top: none;">
		<div class="row-fluid">
			{if !empty($news)}
				<table class="table table-striped">
				 <tr>
				  <th>#</th>
				  <th>Заголовок новости</th>
				  <th>&nbsp;</th>
				  <th style="width:115px;">Дата публикации</th>
				  <th style="width:30px;">&nbsp;</th>
				  <th style="width:30px;">&nbsp;</th>
				  <th style="width:80px;">&nbsp;</th>
				 </tr>
				{foreach from=$news item=news_item}
				 <tr id="tr_{$news_item.id}"{if $news_item.visible == 0} class="info muted"{/if}>
				  <td><a href="?doc_id={$doc_id}&module{if $smarty.get.p}&p={$smarty.get.p}{/if}&action=edit_news&airport={$smarty.get.id}&id={$news_item.id}"{if $news_item.visible == 0} class="info muted"{/if}>{$news_item.id}</a></td>
				  <td><a href="?doc_id={$doc_id}&module{if $smarty.get.p}&p={$smarty.get.p}{/if}&action=edit_news&airport={$smarty.get.id}&id={$news_item.id}"{if $news_item.visible == 0} class="info muted"{/if}>{$news_item.title}</a></td>
				  <td>{if !empty($news_item.onlist)}<i class="icon-ok-sign" title="В общих новостях"></i>{else}&nbsp;{/if}</td>
				  <td>{if ($news_item.publish_date)}{$news_item.publish_date|date_format:"%d.%m.%Y %H:%I"}{/if}</td>
				  <td class="center"><a href="?doc_id={$doc_id}&module&action=set_main_news&id={$news_item.id}" class="btn btn-mini{if $news_item.onmain} btn-info{/if} setMain"><i class="icon-fire"></i></a></td>
				  <td class="center"><span rel="visible" href="#" class="btn btn-mini{if !$news_item.visible} btn-info{/if}" onclick="return visibleItem({$news_item.id})"><i class="{if $news_item.visible}icon-eye-open{else}icon-eye-close icon-white{/if}"></i></span></td>
				  <td class="center"><span onclick="news_delete('{$news_item.id}','{$news_item.title}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td>
				 </tr>
				{/foreach}
				</table>

				{if ($pages.count > 1)}
				  <p class="pager">
				  {section name=pages loop=$pages.count start=0}
					{assign var=pi value="`$smarty.section.pages.iteration - 1`"}
					<a href="?doc_id={$doc_id}&module&p={$pi}" {if ($pi == $pages.current)}style="font-weight: bold;"{/if}>{$pi+1}</a>
				  {/section}
				  </p>
				{/if}

			{else}
				<div class="padding-block"><p>Новости не созданы</p></div>
			{/if}
		</div>
	</div>
</div>

<script type="text/javascript">
function news_delete(id, title) {
	if (confirm('Удалить новость "'+title+'"?')) {
		$.post('?doc_id='+doc_id+'&module&action=delete_news', { id:id }, function(data) {
        	if (data == 1) {
				$('#tr_'+id).fadeOut('fast');
    		}
		} )
	}
}

visibleItem = function(i) {
    $.get('?doc_id={$doc_id}&module&id='+i+'&action=news_visibility', function(r){
        if (r==1) {
	        $('table #tr_'+i+' span[rel=visible]').removeClass('btn-info');
	        $('table #tr_'+i+' span[rel=visible] i').removeClass('icon-eye-close icon-white').addClass('icon-eye-open');
	        $('table #tr_'+i+' ').removeClass('info muted');
            $('table #tr_'+i+' td a').removeClass('muted');
        }
        else {
            $('table #tr_'+i+' span[rel=visible]').addClass('btn-info');
            $('table #tr_'+i+' span[rel=visible] i').addClass('icon-eye-close icon-white').removeClass('icon-eye-open');
            $('table #tr_'+i+' ').addClass('info muted');
            $('table #tr_'+i+' td a').addClass('muted');
        }
    });
}

    $(function() {
        $(document).on('click', '.setMain', function(e) {
            e.preventDefault();
            var self = $(this);
            $.get(self.attr('href'), function(data) {
                if (data.success)
                    self.toggleClass('btn-info')
            }, 'json')
        });
    })
</script>