<div class="padding-block">
    <h3>Аэропорт &laquo;{$airport}&raquo;</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку аэропортов
    </a>
</div>

<form method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=save"  class="form-horizontal">
    <input type="hidden" name="id" value="{$item_page.id}" />
    <input type="hidden" name="airport" value="{$smarty.get.airport}" />
    <div class="padding-block">
        <ul class="nav nav-tabs" style="margin-bottom: 0;" id="sortable">
            <li><a href="?doc_id={$doc_id}&module&action=airport&id={$smarty.get.airport}">Об аэропорте</a></li>
            {foreach $item_sections as $link}
                <li data-id="{$link.id}" {if $link.id == $smarty.get.page}class="active" {/if}><a href="?doc_id={$doc_id}&module&action=edit&airport={$link.id_airport}&page={$link.id}"{if $link.visible == 0} class="muted"{/if}>{$link.name}</a></li>
            {/foreach}
            <li><a href="?doc_id={$doc_id}&module&action=gallery&id={$smarty.get.airport}">Фотогалерея</a></li>
            <li><a href="?doc_id={$doc_id}&module&action=news&id={$smarty.get.airport}">Новости</a></li>
        </ul>

        {if !empty($doc_id)}
        <div class="control-group">

            <div class="ajax-fileuploader-form"{if !empty($head_img)} style="display: none;"{/if}>
                <span class="btn btn-success btn-mini fileinput-button">
                    <i class="icon-picture icon-white"></i>
                    <span>Изображение страницы</span>
                    <input id="fileupload" type="file" name="file" />
                </span>
                <div id="progress" class="progress">
                    <div class="bar"></div>
                </div>
            </div>


            <div id="images" class="ajax-fileuploader-img"{if empty($head_img)} style="display: none;"{/if}>
                <ul class="thumbnails" id="img" style="margin-left: 0;">
                    {if !empty($head_img)}
                    <li>
                        <a class="thumbnail"><img src="{$head_img}"></a>
                        <span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="top_img_delete('{$head_img_name}', this)"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                    </li>
                    {/if}
                </ul>
            </div>
        </div>
        {/if}
    </div>

    <div class="control-group">
        <textarea name="body" id="body">{$item_page.body}</textarea>
    </div>

    <div class="padding-block">

        <div class="control-group">
            <div class="control-label">Название страницы</div>
            <div class="controls">
                <input type="text" name="name" id="name" value="{$item_page.name}" />
                <label class="checkbox" for="visible">
                    <input type="checkbox" name="visible" id="visible" {if $item_page.visible == '0'}checked{/if}/>
                    Скрыть страницу с сайта
                </label>
            </div>
        </div>

        <button class="btn btn-primary b-save" onclick="submit">Сохранить</button>
    </div>
</form>
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('body', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "350" });

        $('#fileupload').fileupload({
            url: '?doc_id={$doc_id}&module&page={$item_page.id}&action=img_upload',
            dataType: 'json',
            done: function (e, data) {
                if (data.result.img) {
                    $('<li><a class="thumbnail"><img src="' + data.result.img + '"></a>' +
                    '<span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="top_img_delete(\'{$head_img_name}\', this)"><i class="icon-white icon-remove"></i> Удалить изображение</span></li>').appendTo('.thumbnails');
                    $('.ajax-fileuploader-form').css({ 'display': 'none' });
                    $('#images').css({ 'display': 'block' });
                }
                if (data.result.success == false) {
                    alert(data.result.error)
                }

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

    function top_img_delete(id, t) {
        if (confirm('Удалить изображение?')) {
            $.post('?doc_id={$doc_id}&module&page={$item_page.id}&action=top_img_delete', { id:id }, function(data) {
                if (data == 1) {
                    $(t).parents('li').fadeOut(function(){ $(t).parents('li').remove(); });
                    $('.ajax-fileuploader-form').css({ 'display': 'block' })
                }
            } )
        }
    }

    $("#sortable").sortable({
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        axis: "x",
        items: "> [data-id]",
        delay: 100,
        stop: function() {
            var data = { ids:[] };
            $('#sortable li[data-id]').each(function(i, el){
                data.ids[i] = $(el).data('id');
            });
            $.post('?doc_id={$doc_id}&module&id={$smarty.get.airport}&action=sort_sections', data);
        }
    }).disableSelection();
</script>