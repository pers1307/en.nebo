<div class="gallery">
    {foreach $albums as $album}
        <div class="photo{if $album@iteration is div by 4} last{/if}">
            <a href="/airports/{$airport.id}/gallery?album={$album.id}">
                {if !empty($album.hash)}
                    <img src="/u/album/{$album.id}/{$album.hash}.{$album.ext}">
                {else}
                    <img src="/i/nophoto_news.jpg">
                {/if}
            </a>
            <p>{$album.title}</p>
        </div>
    {/foreach}

    <br class="clear" />
</div>