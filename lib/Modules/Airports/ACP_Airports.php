<?
class ACP_Airports extends ACP_Module {

    protected $name = __CLASS__;

    private $tree_folder = 'tree/';
    private $path;

    private $preview_folder = 'airports/';
    private $preview_path;
    
    // Gallery
    private $album_folder = 'album/';
    private $album_path;
    protected $thumbSizeX = 230;
    protected $thumbSizeY = 153;

    //News
    private $news_folder = 'news/';
    private $news_path;
    private $limitNews = 10;
    private $news_img_size = array(
        'img_list' => array(230=>'_list'),
        'img_right' => array(450=>'_right'),
        'img_left' => array(450=>'_left'),
    );

    private $news_img_titles = array(
        'img_list' => 'В список',
        'img_right' => 'Справа',
        'img_left' => 'Слева',
    );

    private $errors = array();

    /*
    private $img_size = array(
        'img_top' => array(935=>'air'),
    );

    */

    private $album_img_big = array(
        'photo' => array(1024=>''),
    );


    private $preview_size = array(
        'map' => 180,
        'main' => 280,
    );

    function __construct() {
        parent::__construct();
        $this->path = USER_FILES_DIRECTORY.$this->tree_folder;
        $this->tree_folder = USER_FILES_PATH.$this->tree_folder;
        $this->album_path = USER_FILES_DIRECTORY.$this->album_folder;
        $this->album_folder = USER_FILES_PATH.$this->album_folder;
        $this->preview_path = USER_FILES_DIRECTORY.$this->preview_folder;
        $this->preview_folder = USER_FILES_PATH.$this->preview_folder;
        $this->news_path = USER_FILES_DIRECTORY.$this->news_folder;
        $this->news_folder = USER_FILES_PATH.$this->news_folder;

        $this->smarty->assign('thumbSizeX', $this->thumbSizeX);
        $this->smarty->assign('thumbSizeY', $this->thumbSizeY);
    }

    function rewrite() {
        switch ($this->action) {
            case 'airport':
                $this->airport_item();
                break;

            case 'airport_edit':
                $this->airport_edit();
                break;

			case 'airport_add':
				$this->airport_add();
				break;

			case 'airport_delete':
				$this->airport_delete();
				return false;
				break;

            case 'edit':
                $this->edit_page();
                break;

            case 'gallery':
                $this->airport_gallery();
                break;

            case 'update_album':
                $this->update_album();
                break;

            case 'edit_album':
                $this->edit_album();
                break;

            case 'delete_album':
                $this->delete_album();
                break;

            case 'upload_photo':
                $this->upload_photo();
                return false;
                break;

            case 'delete_photo':
                $this->delete_photo();
                return false;
                break;

            case 'main_photo':
                $this->main_photo();
                return false;
                break;

            case 'rotate_photo':
                $this->rotate_photo();
                return false;
                break;

            case 'crop_photo':
                $this->crop_photo();
                return false;
                break;

            case 'save':
                $this->save_page();
                break;

            case 'img_upload':
                $this->img_upload();
                return false;
                break;

            case 'top_img_delete':
                $this->top_img_delete();
                return false;
                break;

            case 'item_visibility':
                $this->item_visibility();
                return false;
                break;

            case 'edit_map':
                $this->edit_map();
                return false;
                break;

            case 'photo_rename':
                $this->photo_rename();
                return false;
                break;

            case 'news':
                $this->airport_news();
                break;

            case 'edit_news':
                $this->edit_news();
                break;

            case 'save_news':
                $this->save_news();
                break;

            case 'delete_news':
                $this->delete_news();
                return false;
                break;

            case 'upload_images_news':
                $this->upload_images_news(Utils::parseGet('id'), $this->news_img_size);
                return false;
                break;

            case 'img_delete_news':
                $this->delete_img_news();
                return false;
                break;

            case 'news_visibility':
                $this->news_visibility();
                return false;
                break;

            case 'set_main_news':
                $this->set_main_news();
                return false;
                break;

            case 'sort':
                $this->sort();
                return false;
                break;

            case 'sort_sections':
                $this->sort_sections();
                return false;
                break;

            case 'sort_thumb_albums':
                $this->sort_thumb_albums();
                return false;
                break;

            case 'sort_thumb_photos':
                $this->sort_thumb_photos();
                return false;
                break;

            default:
                $this->show_air_list();
                break;
        }
        return true;
    }

    private function show_air_list() {
        $airports = $this->db->getAll("SELECT * FROM airports ORDER BY sort ASC");
        $points = $this->db->getAll("SELECT * FROM airports WHERE map_position <> 0 AND visible = 1 ORDER BY id ASC");

        array_walk($points, function(&$item) {
            $item['position'] = explode(',', $item['map_position']);
        });

        $this->smarty->assign('airports', $airports);
        $this->smarty->assign('points', $points);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_list.tpl');
    }

	private function airport_add() {
		$arr = array();
		$arr['title'] = stripslashes(trim($_POST['title']));
		$arr['full_title'] = stripslashes(trim($_POST['full_title']));
		$arr['description'] = stripslashes(trim($_POST['description']));
		$arr['description_main'] = stripslashes(trim($_POST['description_main']));
		$arr['contacts'] = stripslashes(trim($_POST['contacts']));
		$arr['email'] = stripslashes(trim($_POST['email']));
		$arr['url'] = stripslashes(trim($_POST['url']));
		$arr['flag'] = stripslashes(trim($_POST['flag']));
		$arr['emails'] = stripslashes(trim($_POST['emails']));
		$arr['city'] = stripslashes(trim($_POST['city']));

		$this->smarty->assign('item_info', $arr);

		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_add.tpl');
	}

    private function airport_item() {
        $id = Utils::parseGet('id');
        $item_info = $this->db->getRow("SELECT * FROM airports WHERE id = {$id}"); //Инфа об аэропорте

        $item_sections = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = {$id} ORDER BY sort ASC"); //Выбираем разделы текущего аэропорта

        $img_map = USER_FILES_DIRECTORY.'airports/map_'.$id.'.jpg';
        if (file_exists($img_map)){
            $img_map = USER_FILES_PATH.'airports/map_'.$id.'.jpg';
            $this->smarty->assign('img_map', $img_map);
        }

        $img_main = USER_FILES_DIRECTORY.'airports/main_'.$id.'.jpg';
        if (file_exists($img_main)){
            $img_main = USER_FILES_PATH.'airports/main_'.$id.'.jpg';
            $this->smarty->assign('img_main', $img_main);
        }

        $this->smarty->assign('item_info', $item_info);
        $this->smarty->assign('item_sections', $item_sections);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_item.tpl');
    }

    private function airport_edit(){
        $airport_id = intval($_POST['id']);

        $arr = array();
        $arr['title'] = stripslashes(trim($_POST['title']));
        $arr['full_title'] = stripslashes(trim($_POST['full_title']));
        $arr['description'] = stripslashes(trim($_POST['description']));
        $arr['description_main'] = stripslashes(trim($_POST['description_main']));
        $arr['contacts'] = stripslashes(trim($_POST['contacts']));
        $arr['email'] = stripslashes(trim($_POST['email']));
        $arr['url'] = stripslashes(trim($_POST['url']));
        $arr['flag'] = stripslashes(trim($_POST['flag']));
        $arr['emails'] = stripslashes(trim($_POST['emails']));
        $arr['city'] = stripslashes(trim($_POST['city']));


        if ($airport_id) {
            $this->db->update('airports', $arr, "id = $airport_id");
        } else {
            $airport_id = $this->db->insert('airports', $arr);
        }
        foreach ($this->preview_size as $k => $v) {
            $this->upload_file('file_' . $k, $airport_id, $v);
        }

		if(!isset($_POST['id'])) {
			$this->add_section_page($airport_id);
		}

        header("Location: /_bo/?doc_id={$this->doc_id}&module&action=airport&id={$airport_id}");
        exit;
    }

	private function add_section_page($air_id) {
		$pages = array(
			array(
				'id_airport' => $air_id,
				'name' => 'Актуальные показатели',
				'body' => '<p></p>',
				'visible' => 0,
				'sort' => 1
			),
			array(
				'id_airport' => $air_id,
				'name' => 'История аэрапорта',
				'body' => '<p></p>',
				'visible' => 0,
				'sort' => 2
			),
			array(
				'id_airport' => $air_id,
				'name' => 'Планы по развитию',
				'body' => '<p></p>',
				'visible' => 0,
				'sort' => 3
			)
		);

		foreach ($pages as $page) {
			$this->db->insert('airports_section', $page);
		}
	}

	private function airport_delete() {
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$this->db->delete('airports_section', "id_airport = $id");
		$this->db->delete('airports', "id = $id");

		echo 1;
	}

    private function edit_page() {
        $id = Utils::parseGet('doc_id');
        $airport_id = Utils::parseGet('airport');
        $page_id = Utils::parseGet('page');

        $airport = $this->db->getOne("SELECT title FROM airports WHERE id = {$airport_id}");
        $item_page = $this->db->getRow("SELECT * FROM airports_section WHERE id = {$page_id}");
        $item_sections = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = {$airport_id} ORDER BY sort ASC"); //Выбираем разделы текущего аэропорта

        /* Изображение для страницы */
        $img_head_name = $id . '_' . $page_id . '_air';
        $head_img = str_replace(ROOT, URL, Utils::findImage($img_head_name, ROOT.$this->tree_folder));
        $head_img_src = $img_head_name .'.jpg';

        $this->smarty->assign('airport', $airport);
        $this->smarty->assign('item_page', $item_page);
        $this->smarty->assign('item_sections', $item_sections);
        $this->smarty->assign('head_img', $head_img);
        $this->smarty->assign('head_img_src', $head_img_src);
        $this->smarty->assign('head_img_name', $img_head_name);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_page.tpl');


    }

    private function airport_gallery() {
        $id = Utils::parseGet('id');

        $item_info = $this->db->getRow("SELECT * FROM airports WHERE id = {$id}"); //Инфа об аэропорте
        $item_sections = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = {$id} ORDER BY sort ASC"); //Выбираем разделы текущего аэропорта
        $albums = $this->db->getAll("SELECT al.*, aph.hash, aph.ext, aph.on_main FROM airport_albums AS al LEFT JOIN airport_photos AS aph ON al.id = aph.id_album AND aph.on_main = 1 WHERE al.id_airport = {$id} ORDER BY sort");

        $this->smarty->assign('item_info', $item_info);
        $this->smarty->assign('item_sections', $item_sections);
        $this->smarty->assign('albums', $albums);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_gallery.tpl');
    }

    private function update_album() {
        $id = Utils::parseGet('id');
        $title = Utils::parsePost('title', false);
        $album_id = Utils::parsePost('album');
        $item_info = $this->db->getRow("SELECT * FROM airports WHERE id = {$id}");
        if ($album_id)
            $album = $this->db->getRow("SELECT * FROM airport_albums WHERE id = ?", array($album_id));
        if ($item_info && $title) {
            $data = array(
                'id_airport' => $id,
                'title' => $title,
            );
            if (isset($album) && $album) {
                $this->db->update('airport_albums', $data, "id = {$album_id}");
            } else {
                $this->db->insert('airport_albums', $data);
            }
        }
        header("Location: /_bo/?doc_id={$this->doc_id}&module&action=gallery&id={$id}");
    }

    private function upload_file($name, $id, $size)
    {
        if (!empty($_FILES[$name]) && $_FILES[$name]['error'] == 0) {
            $img = Image::CreateFromFile($_FILES[$name]['tmp_name']);
            if($name == 'file_main') {
              $img->resize_main($size);
            } else
              $img->resize($size);
            $name = str_replace('file_', '', $name);
            if (!file_exists($this->preview_path))
                Tools::create_directory($this->preview_path, 0777);
            $img->save("{$this->preview_path}{$name}_{$id}.jpg", 'jpeg');
        }
    }

    private function edit_album() {
        $id = Utils::parseGet('id');
        //$item_info = $this->db->getRow("SELECT * FROM airport_albums WHERE id = {$id}");
        $item_info = $this->db->getRow("SELECT al.id, al.id_airport, al.title, aph.hash FROM airport_albums AS al LEFT JOIN airport_photos AS aph ON al.id = aph.id_album AND aph.on_main = 1 WHERE al.id = {$id}");

        if (!$item_info)
            header("Location: /_bo/?doc_id={$this->doc_id}&module&action=gallery&id={$id}");

        $photos = $this->db->getAll("SELECT * FROM airport_photos WHERE id_album = {$id} ORDER BY sort");

        $this->smarty->assign('photos', $photos);
        $this->smarty->assign('item_info', $item_info);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_gallery_album.tpl');
    }

    private function delete_album()
    {
        $id = Utils::parsePost('id');
        $this->db->delete('airport_albums', "id = {$id}");
        $photos = glob($this->album_path . $id . '/*.*');
        foreach ($photos as $photo) {
            if (file_exists($photo))
                unlink($photo);
        }
        rmdir($this->album_path . $id);
    }

    private function upload_photo()
    {
        $id = Utils::parseGet('id');
        $output = array();

        foreach ($this->album_img_big as $key=>$sizes) {

            if (!isset($_FILES[$key]) or empty($_FILES[$key]['tmp_name']) or $_FILES[$key]['error']) continue;


            $tmp_name = $_FILES[$key]['tmp_name'];
            $file_name = $_FILES[$key]['name'];

            $dest_path = $this->album_path.$id.'/';

            if (!file_exists($dest_path)) {
                Tools::create_directory($dest_path, 0777);
            }
            if (!is_writeable($dest_path)) continue;

            $e = strrpos($file_name, '.') + 1;
            if ($e == 1) continue;

            $ext = substr($file_name, $e);
            if ($ext == 'jpeg') $ext = 'jpg';

            if (!in_array($ext, array('jpg', 'png', 'gif'))) continue;

            // convert to jpg
            $ext = 'jpg';

            $hash = md5($file_name);
            $file = $dest_path.$hash.'.'.$ext;

            if (file_exists($file)) {
                unlink($file);
            }

            rename($tmp_name, $file);
            @chmod($file, 0755);


            if (is_array($sizes) and count($sizes)) {
                $Img = Image::CreateFromFile($file);
                foreach ($sizes as $w=>$prfx) {
                    $new_file_name = $dest_path.md5($file_name).$prfx;
                    $new_file = $new_file_name.'.'.$ext;
                    $new_file_thumb = $new_file_name.'_thumb.'.$ext;
                    $Img->resize($w);
                    $Img->save($new_file, 'jpeg');
                    $Img->resize(230,230);
                    $Img->save($new_file_thumb, 'jpeg');
                }
            }

            $id_album = $id;
            $this->db->insert('airport_photos', compact('id_album', 'hash', 'ext'));

            $output = $this->db->getRow("SELECT id FROM airport_photos WHERE hash = '{$hash}'");
            $output['img'] = $this->album_folder.$id.'/'.$hash.'_thumb.'.$ext;

        }
        echo json_encode($output);
    }

    protected function delete_photo()
    {
        $success = false;
        $id = intval($_POST['id']);
        $photo = $this->db->getRow("SELECT * FROM airport_photos WHERE id = {$id}");
        $path = $this->album_path.$photo['id_album'].'/'.$photo['hash'].'.'.$photo['ext'];
        $path_thumb = $this->album_path.$photo['id_album'].'/'.$photo['hash'].'_thumb.'.$photo['ext'];

        if (file_exists($path))
            $success = unlink($path);
        if (file_exists($path_thumb))
            $success = unlink($path_thumb);

        $this->db->delete('airport_photos', "id = {$id}");
        echo json_encode(array('success' => $success));
    }

    protected function main_photo()
    {
        $album_id = Utils::parseGet('album_id');
        if (isset($_GET['photo']))
            $photo = $_GET['photo'];

        if ($album_id && isset($photo) && $photo) {
                $album = $this->db->getRow("SELECT id, id_album, on_main FROM airport_photos WHERE id = ?", array($photo));

                if ($album) {
                    //$this->db->update('airport_photos', array('on_main'=>($album['on_main'] ? 0:1)), 'id='.$photo);
                    $this->db->update('airport_photos', array('on_main' => 0), "id_album = {$album_id}");
                    $this->db->update('airport_photos', array('on_main' => 1), "id = {$photo}");
                    echo json_encode(array('success' => true));
            }
        }
    }

    private function rotate_photo()
    {
        if (isset($_GET['photo']) && $photo = $_GET['photo']) {
            $file = str_replace(USER_FILES_PATH, USER_FILES_DIRECTORY, $photo);
            if (file_exists($file)) {
                // rotate main image
                $image = imagecreatefromstring(file_get_contents($file));
                $rotate = imagerotate($image, -90, 0);

                imagejpeg($rotate, $file, 100);
                imagedestroy($image);
                unset($rotate, $image, $fn);

                echo json_encode(array('success' => true));
            }
        }
    }

    private function crop_photo()
    {
        if (isset($_POST['photo']) && $photo = $_POST['photo']) {
            $link = $_POST['link'];
            $ext = $_POST['ext'];
            $select = $_POST['select'];
            $select = array_map(function($e) { return round(floatval($e)); }, $select);

            $file = str_replace(USER_FILES_PATH, USER_FILES_DIRECTORY, $link.$photo.'.'.$ext);
            $thumb = str_replace(USER_FILES_PATH, USER_FILES_DIRECTORY, $link.$photo.'_thumb.'.$ext);
            if (file_exists($file)) {
                $image = imagecreatefromjpeg($file);
                $cropped = imagecreatetruecolor($this->thumbSizeX, $this->thumbSizeY);
                imagecopyresampled($cropped, $image, 0, 0, $select['x'], $select['y'], $this->thumbSizeX, $this->thumbSizeY, $select['w'], $select['h']);
                imagejpeg($cropped, $thumb, 100);
                imagedestroy($image);
                imagedestroy($cropped);
                echo json_encode(array('success' => true));
            }
        }
    }


    private function save_page() {
        if (empty($_POST)) {
            header("Location: /_bo/?doc_id={$this->doc_id}&module");
            exit;
        }
        $id = intval($_POST['id']);
        $airport = intval($_POST['airport']);


        $arr = array();
        $arr['name'] = stripslashes(trim($_POST['name']));
        $arr['body'] = stripslashes(trim($_POST['body']));
        $arr['visible'] = ($_POST['visible'] == 'on') ? 0 : 1;
        //$arr['create_date'] = time();
        //$arr['publish_date'] = Tools::date_to_timestamp($_POST['publish_date'], '.');

        if (!$arr['body']) {
            header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
            exit;
        }

        if ($id) {
            $this->db->update('airports_section', $arr, "id = $id");
        } else {
            $id = $this->db->insert('airports_section', $arr);
        }

        //$img_ext = $this->upload_images_tree($id, $this->news_img_size);


        header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&airport={$airport}&page={$id}");
        exit;
    }

    private function img_upload() {

        $id = intval($_GET['doc_id']);
        $page = intval($_GET['page']);

        //config
        $img_w = '935';
        $img_h = '240';


        if (isset($_FILES['file'])) {

            //get file ext
            $file_ext = strrchr($_FILES['file']['name'], '.');

            //get file rename
            $file = $this->path . $id . '_' . $page . '_air' . $file_ext;
            $file_name = $this->tree_folder . $id . '_' . $page . '_air' . $file_ext;
            $uploaded_file = $_FILES['file']['tmp_name'];

            //check if its allowed or not
            $whitelist = array('.jpg','.jpeg','.png');
            if (!(in_array($file_ext, $whitelist))) {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }

            //check upload type
            $pos = strpos($_FILES['file']['type'],'image');
            if($pos === false) {
                echo json_encode(array('success' => false, 'error' => 'Файл не является картинкой'));
                return;
            }

            $imageinfo = getimagesize($uploaded_file);

            //check size image
            if ($imageinfo[0] < $img_w || $imageinfo[1] < $img_h) {
                echo json_encode(array('success' => false, 'error' => 'Размер изображения должен быть не менее 935x240 пикселей'));
                return;
            }

            if ($imageinfo['mime'] != 'image/jpeg'&& $imageinfo['mime'] != 'image/jpg'&& $imageinfo['mime'] != 'image/png') {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }


            //check directory
            if (!file_exists($this->path))
                Tools::create_directory($this->path, 0777);


            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                $img = Image::CreateFromFile($uploaded_file);
                $img->resize($img_w);

                //check image height
                if ($img->height < $img_h) {
                    echo json_encode(array('success' => false, 'error' => 'Соотношение сторон изображения должно быть не менее, чем 1:4. Минимальный размер 935x240.'));
                    return;
                }

                $img->crop(0,0,$img_w,$img_h);
                $img->save($file, 'jpeg');
            } else {
                echo json_encode(array('success' => false, 'error' => 'Ошибка загрузки файла. Попробуйте еще раз.'));
                return;
            }

            echo json_encode(array('success' => true, 'link' => $file, 'img' => $file_name));
        }
    }

    private function top_img_delete() {

        $id = $_POST['id'];

        if (empty($id)) return false;
        $img = Utils::findImage($id, ROOT.$this->tree_folder);

        if ($img) unlink($img);
        exit('1');
    }

    public function item_visibility() {
        $id = Utils::parseGet('id');
        $item = $this->db->getRow("SELECT * FROM airports WHERE id=".(int)$id);
        if (!empty($item)) {
            $this->db->update('airports', array('visible'=>($item['visible'] ? 0:1)), 'id='.$id);
            echo (int)!$item['visible'];
            exit();
        }
    }

    private function edit_map()
    {
        $id = Utils::parsePost('id');
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM airports WHERE id = ? AND visible = 1", array($id));
            if (isset($_POST['map_position']) && $item) {
                $this->db->update('airports', array('map_position' => $_POST['map_position']), "id = $id");
                exit(json_encode(array('success' => true)));
            }
        }
        echo json_encode(array('success' => false));
    }

    private function photo_rename() {
        if (!isset($_POST['id']) or !isset($_POST['title'])) exit;
        $id = intval($_POST['id']);
        $title = trim($_POST['title']);

        if (!$id or !$title) exit;

        $this->db->update('airport_photos', compact('title'), "id = {$id}");
        echo 1;
    }

    private function airport_news() {
        $id = Utils::parseGet('id');
        
        $count = $this->db->getOne("SELECT COUNT(*) FROM news WHERE airport_id = ".$id);
        $limit = $this->limitNews;
        $p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;
        $limit_from =  $limit * $p;
        $count = ceil($count / $limit);
        $this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

        $item_info = $this->db->getRow("SELECT * FROM airports WHERE id = {$id}"); //Инфа об аэропорте
        $item_sections = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = {$id} ORDER BY sort ASC"); //Выбираем разделы текущего аэропорта
        $news = $this->db->getAll("SELECT * FROM news WHERE airport_id = ".$id." ORDER BY publish_date DESC LIMIT $limit_from, $limit");

        $this->smarty->assign('item_info', $item_info);
        $this->smarty->assign('item_sections', $item_sections);
        $this->smarty->assign('news', $news);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_news_list.tpl');
    }

    private function edit_news() {
        if ((isset($_GET['id'])) and ($id = intval($_GET['id']))) {
            $news = $this->db->getRow("SELECT * FROM news WHERE id = {$id}");
            foreach ($this->news_img_size as $name => $sizes) {
                foreach ($sizes as $prefix) {
                    $url = $id . $prefix . '.jpg';
                    if (file_exists($this->news_path . $url)) {
                        $news[$name] = $this->news_folder . $url;
                    }
                }
            }
            $news['video'] = array();
            foreach (glob($this->news_path . "{$news['id']}_video.*") as $file) {
                $type = substr($file, -3);
                $link = str_replace($this->news_path, $this->news_folder, $file);
                if ($type == 'jpg')
                    $news['thumb'] = $link;
                else
                    $news['video'][$type] = $link;
            }

        } else {
            $publish_date = date("d.m.Y");
            $news = compact('publish_date');
        }
        

        $this->smarty->assign('news_item', $news);
        $this->smarty->assign('img_sizes', $this->news_img_size);
        $this->smarty->assign('img_titles', $this->news_img_titles);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_air_news_edit.tpl');
    }

    private function save_news() {
        if (empty($_POST)) {
            header("Location: /_bo/?doc_id={$this->doc_id}&module");
            exit;
        }
        $id = intval($_POST['id']);
        $airport = intval($_POST['airport']);

        $arr = array();
        $arr['title'] = htmlspecialchars(trim($_POST['title']));
        $arr['short_text'] = htmlspecialchars(trim($_POST['short_text']));
        $arr['full_text'] = stripslashes(trim($_POST['full_text']));
        $arr['create_date'] = time();
        $arr['publish_date'] = Tools::date_to_timestamp($_POST['publish_date'], '.');
        $arr['airport_id'] = $airport;
        $arr['onlist'] = isset($_POST['onlist']) ? 1 : 0;

        if (!$arr['title']) {
            $this->errors[] = 'Не заполнено название';
        }

        $this->check_video();

        if (empty($this->errors)) {
            if ($id) {
                $this->db->update('news', $arr, "id = $id");
                $this->upload_images_news($id, $this->news_img_size);
                if (!$this->upload_video_news($id)) {
                    if (isset($_POST['time'])) {
                        $file = "{$this->news_path}{$id}_video";
                        if (file_exists($file . '.mp4'))
                            $this->create_thumb("{$file}.mp4", "{$file}.jpg", false);
                    }
                }
            } else {
                $id = $this->db->insert('news', $arr);
            }
        }

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
            echo json_encode(array('success' => empty($this->errors), 'errors' => $this->errors));
        else
            header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit_news&airport={$airport}&id={$id}");
        exit;
    }

    private function upload_images_news($id, $key_arr = array()) {
        $output = array();
        foreach ($key_arr as $key=>$sizes) {
            if (!isset($_FILES[$key]) or empty($_FILES[$key]['tmp_name']) or $_FILES[$key]['error']) continue;

            $tmp_name = $_FILES[$key]['tmp_name'];
            $file_name = $_FILES[$key]['name'];

            $dest_path = $this->news_path;

            if (!file_exists($dest_path)) {
                Tools::create_directory($dest_path, 0777);
            }
            if (!is_writeable($dest_path)) continue;

            $e = strrpos($file_name, '.') + 1;
            if ($e == 1) continue;

            $ext = substr($file_name, $e);
            if ($ext == 'jpeg') $ext = 'jpg';

            if (!in_array($ext, array('jpg', 'png', 'gif'))) continue;

            // convert to jpg
            $ext = 'jpg';

            $file = $dest_path.$id.'.'.$ext;

            if (file_exists($file)) {
                unlink($file);
            }

            rename($tmp_name, $file);
            @chmod($file, 0755);

            if (is_array($sizes) and count($sizes)) {
                $Img = Image::CreateFromFile($file);
                foreach ($sizes as $w=>$prfx) {
                    $new_file = $dest_path.$id.$prfx.'.'.$ext;
                    $Img->resize($w);
                    $Img->save($new_file, 'jpeg');
                }
            }

            $output['img'] = $ext;
        }
        return $output;
    }

    private function check_video()
    {
        if (isset($_FILES['video']) && $_FILES['video']['error'] === 0 && ($file = $_FILES['video'])) {
            $type = substr($file['type'], -3);
            if (!in_array($type, array('flv', 'mp4')))
                $this->errors[] = 'Неверный формат видео';
        }
    }

    private function upload_video_news($id)
    {
        $result = false;
        $name = 'video';
        $type = substr($_FILES[$name]['type'], -3);
        if (isset($_FILES[$name]) && $_FILES[$name]['error'] === 0) {
            $path = $this->news_path;
            if (!file_exists($path)) {
                Tools::create_directory($path, 0777);
            }
            $file = "{$path}{$id}_video.{$type}";

            $content_range = preg_split('/[^0-9]+/', isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : '');
            $append_file = isset($content_range[1]) && $content_range[1] > 0;
            $uploaded_file = $_FILES[$name]['tmp_name'];
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                    $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
                } else {
                    $result = move_uploaded_file($uploaded_file, $file) && !isset($content_range[3]);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
                $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
            }
            if ($result && $type == 'mp4') {
                $this->create_thumb($file, str_replace('mp4', 'jpg', $file));
            }
        }
        return $result;
    }

    private function create_thumb($video, $image, $rand = true)
    {
        $ffmpeg = FFMPEG;

        if (isset($_POST['time']) && preg_match('/\d{2}:\d{2}:\d{2}/', $_POST['time']) && $_POST['time'] != '00:00:00') {
            $time = $_POST['time'];
        } elseif ($rand) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} 2>&1");
            $duration = ob_get_clean();
            preg_match('/Duration: (.*?),/', $duration, $matches);
            $duration = $matches[1];
            $duration_array = explode(':', $duration);
            $duration = $duration_array[0] * 3600 + $duration_array[1] * 60 + $duration_array[2];
            $time = gmdate('H:i:s.u', mt_rand(1, $duration - 1));
        }

        if (isset($time) && file_exists($video)) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} -ss {$time} -f image2 -vframes 1 {$image}");
            ob_end_clean();
        }
    }

    private function delete_news(){
        if (!isset($_POST['id'])) exit;
        $id = intval($_POST['id']);
        if (!$id) exit;
        $this->db->delete('news', "id = $id");

        echo 1;
    }


    private function delete_img_news() {
        if (!isset($_POST['id']) or !isset($_POST['imgkey'])) exit;
        $id = intval($_POST['id']);
        $imgkey = trim($_POST['imgkey']);

        if (!$id or !isset($this->news_img_size[$imgkey])) exit;

        $prfxs = $this->news_img_size[$imgkey];
        if (is_array($prfxs)) {
            foreach ($prfxs as $prfx) {
                $file = $this->news_path."{$id}{$prfx}.jpg";
                if (file_exists($file)) {
                    @unlink($file);
                }
            }
        } else {
            $file = $this->news_path."{$id}.jpg";
            if (file_exists($file)) {
                @unlink($file);
            }
        }
        echo 1;
    }

    private function news_visibility() {
        $id = Utils::parseGet('id');
        $item = $this->db->getRow("SELECT * FROM news WHERE id=".(int)$id);
        if (!empty($item)) {
            $this->db->update('news', array('visible'=>($item['visible'] ? 0:1)), 'id='.(int)$id);
            echo (int)!$item['visible'];
            exit();
        }
    }

    private function set_main_news()
    {
        $id = Utils::parseGet('id');
        if ($id)
            $ret = $this->db->query("UPDATE news SET onmain = !onmain WHERE id = ".(int)$id);
        echo json_encode(array('success' => isset($ret) && !PEAR::isError($ret)));
    }

    private function sort()
    {
        if (!empty($_POST['ids'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->simpleQuery("
            UPDATE airports SET sort = (@sort:=@sort + 1)
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ");
            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

    private function sort_sections()
    {
        if (!empty($_POST['ids']) && !empty($_GET['id'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->query("
            UPDATE airports_section SET sort = (@sort:=@sort + 1)
            WHERE id_airport = ?
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ", array($_GET['id']));
            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

    private function sort_thumb_albums ()
    {
        if (!empty($_POST['ids']) && !empty($_GET['id'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->query("
            UPDATE airport_albums SET sort = (@sort:=@sort + 1)
            WHERE id_airport = ?
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ", array($_GET['id']));

            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

    private function sort_thumb_photos ()
    {
        if (!empty($_POST['ids']) && !empty($_GET['id'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->query("
            UPDATE airport_photos SET sort = (@sort:=@sort + 1)
            WHERE id_album = ?
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ", array($_GET['id']));

            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }
}
