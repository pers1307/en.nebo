<?
class ACP_News extends ACP_Module {

	protected $name = __CLASS__;

	private $folder = 'news/';
	private $path;

	private $limit = 10;

	private $img_size = array(
        'img_list' => array(230=>'_list'),
        'img_right' => array(450=>'_right'),
        'img_left' => array(450=>'_left'),
    );

    private $img_titles = array(
        'img_list' => 'В список',
        'img_right' => 'Справа',
        'img_left' => 'Слева',
    );

    private $errors = array();

	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;
	}

	function rewrite() {
		switch ($this->action) {
			case 'edit':
				$this->edit_news();
				break;

			case 'save':
				$this->save_news();
				break;

			case 'delete':
				$this->delete_news();
				return false;
				break;

            case 'upload_images':
                $this->upload_images(Utils::parseGet('id'), $this->img_size);
                return false;
                break;

            case 'img_delete':
				$this->delete_img();
				return false;
				break;

            case 'item_visibility':
				$this->item_visibility();
				return false;
				break;

            case 'set_main':
                $this->set_main();
                return false;
                break;

 			default:
 				$this->show_list();
 				break;
		}
		return true;
	}

	function fix_slashes() {
		$list = $this->db->getAll("SELECT id, full_text FROM news");
		foreach ($list as $row) {
			extract($row);
			$c = mb_strlen($full_text);
			$full_text = stripslashes($full_text);
			$c2 = mb_strlen($full_text);
			$this->db->update('news', compact('full_text'), "id = {$id}");
//			echo "[{$id} {$c} &rarr; {$c2}]<br>";
		}
	}

	private function show_list() {
		$count = $this->db->getOne("SELECT COUNT(*) FROM news WHERE airport_id IS NULL");
		$limit = $this->limit;
		$p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;
		$limit_from =  $limit * $p;
		$count = ceil($count / $limit);
		$this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

		$news = $this->db->getAll("SELECT * FROM news WHERE airport_id IS NULL ORDER BY publish_date DESC LIMIT $limit_from, $limit");
		$this->smarty->assign('news', $news);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_news_list.tpl');
	}

	private function edit_news() {
		if ((isset($_GET['id'])) and ($id = intval($_GET['id']))) {
			$news = $this->db->getRow("SELECT * FROM news WHERE id = {$id}");
            foreach ($this->img_size as $name => $sizes) {
                foreach ($sizes as $prefix) {
                    $url = $id . $prefix . '.jpg';
                    if (file_exists($this->path . $url)) {
                        $news[$name] = $this->folder . $url;
                    }
                }
            }
            $news['video'] = array();
            foreach (glob($this->path . "{$news['id']}_video.*") as $file) {
                $type = substr($file, -3);
                $link = str_replace($this->path, $this->folder, $file);
                if ($type == 'jpg')
                    $news['thumb'] = $link;
                else
                    $news['video'][$type] = $link;
            }

		} else {
			$publish_date = date("d.m.Y");
			$news = compact('publish_date');
		}

		$this->smarty->assign('news_item', $news);
        $this->smarty->assign('img_sizes', $this->img_size);
        $this->smarty->assign('img_titles', $this->img_titles);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_news_edit.tpl');
	}

	private function save_news() {
		if (empty($_POST)) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module");
			exit;
		}
		$id = intval($_POST['id']);

		$arr = array();
		$arr['title'] = htmlspecialchars(trim($_POST['title']));
		$arr['short_text'] = htmlspecialchars(trim($_POST['short_text']));
		$arr['full_text'] = stripslashes(trim($_POST['full_text']));
		$arr['create_date'] = time();
		$arr['publish_date'] = Tools::date_to_timestamp($_POST['publish_date'], '.');

		if (!$arr['title']) {
            $this->errors[] = 'Не заполнено название';
		}

        $this->check_video();

        if (empty($this->errors)) {
            if ($id) {
                $this->db->update('news', $arr, "id = $id");
                $this->upload_images($id, $this->img_size);
                if (!$this->upload_video($id)) {
                    if (isset($_POST['time'])) {
                        $file = "{$this->path}{$id}_video";
                        if (file_exists($file . '.mp4'))
                            $this->create_thumb("{$file}.mp4", "{$file}.jpg", false);
                    }
                }
            } else {
                $id = $this->db->insert('news', $arr);
            }
        }

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		    echo json_encode(array('success' => empty($this->errors), 'errors' => $this->errors));
        else
            header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
        exit;
	}

	private function upload_images($id, $key_arr = array()) {
		$output = array();
		foreach ($key_arr as $key=>$sizes) {
			if (!isset($_FILES[$key]) or empty($_FILES[$key]['tmp_name']) or $_FILES[$key]['error']) continue;

			$tmp_name = $_FILES[$key]['tmp_name'];
			$file_name = $_FILES[$key]['name'];

			$dest_path = $this->path;

			if (!file_exists($dest_path)) {
				Tools::create_directory($dest_path, 0777);
			}
			if (!is_writeable($dest_path)) continue;

			$e = strrpos($file_name, '.') + 1;
			if ($e == 1) continue;

			$ext = substr($file_name, $e);
			if ($ext == 'jpeg') $ext = 'jpg';

			if (!in_array($ext, array('jpg', 'png', 'gif'))) continue;

            // convert to jpg
            $ext = 'jpg';

			$file = $dest_path.$id.'.'.$ext;

			if (file_exists($file)) {
				unlink($file);
			}

			rename($tmp_name, $file);
			@chmod($file, 0755);

			if (is_array($sizes) and count($sizes)) {
				$Img = Image::CreateFromFile($file);
				foreach ($sizes as $w=>$prfx) {
					$new_file = $dest_path.$id.$prfx.'.'.$ext;
					$Img->resize($w);
					$Img->save($new_file, 'jpeg');
				}
			}

			$output['img'] = $ext;
		}
		return $output;
	}

    private function check_video()
    {
        if (isset($_FILES['video']) && $_FILES['video']['error'] === 0 && ($file = $_FILES['video'])) {
            $type = substr($file['type'], -3);
            if (!in_array($type, array('flv', 'mp4')))
                $this->errors[] = 'Неверный формат видео';
        }
    }

    private function upload_video($id)
    {
        $result = false;
        $name = 'video';
        $type = substr($_FILES[$name]['type'], -3);
        if (isset($_FILES[$name]) && $_FILES[$name]['error'] === 0) {
            $path = $this->path;
            if (!file_exists($path)) {
                Tools::create_directory($path, 0777);
            }
            $file = "{$path}{$id}_video.{$type}";

            $content_range = preg_split('/[^0-9]+/', isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : '');
            $append_file = isset($content_range[1]) && $content_range[1] > 0;
            $uploaded_file = $_FILES[$name]['tmp_name'];
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                    $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
                } else {
                    $result = move_uploaded_file($uploaded_file, $file) && !isset($content_range[3]);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
                $result = !isset($content_range[3]) || $content_range[2] == $content_range[3] - 1;
            }
            if ($result && $type == 'mp4') {
                $this->create_thumb($file, str_replace('mp4', 'jpg', $file));
            }
        }
        return $result;
    }

    private function create_thumb($video, $image, $rand = true)
    {
        $ffmpeg = FFMPEG;

        if (isset($_POST['time']) && preg_match('/\d{2}:\d{2}:\d{2}/', $_POST['time']) && $_POST['time'] != '00:00:00') {
            $time = $_POST['time'];
        } elseif ($rand) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} 2>&1");
            $duration = ob_get_clean();
            preg_match('/Duration: (.*?),/', $duration, $matches);
            $duration = $matches[1];
            $duration_array = explode(':', $duration);
            $duration = $duration_array[0] * 3600 + $duration_array[1] * 60 + $duration_array[2];
            $time = gmdate('H:i:s.u', mt_rand(1, $duration - 1));
        }

        if (isset($time) && file_exists($video)) {
            ob_start();
            passthru("{$ffmpeg} -i {$video} -ss {$time} -f image2 -vframes 1 {$image}");
            ob_end_clean();
        }
    }

	private function delete_news(){
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$this->db->delete('news', "id = $id");

		echo 1;
	}


	function delete_img() {
		if (!isset($_POST['id']) or !isset($_POST['imgkey'])) exit;
		$id = intval($_POST['id']);
		$imgkey = trim($_POST['imgkey']);

		if (!$id or !isset($this->img_size[$imgkey])) exit;

        $prfxs = $this->img_size[$imgkey];
        if (is_array($prfxs)) {
            foreach ($prfxs as $prfx) {
                $file = $this->path."{$id}{$prfx}.jpg";
                if (file_exists($file)) {
                    @unlink($file);
                }
            }
        } else {
            $file = $this->path."{$id}.jpg";
            if (file_exists($file)) {
                @unlink($file);
            }
        }
		echo 1;
	}

    public function item_visibility() {
		$id = Utils::parseGet('id');
		$item = $this->db->getRow("SELECT * FROM news WHERE id=".(int)$id);
		if (!empty($item)) {
			$this->db->update('news', array('visible'=>($item['visible'] ? 0:1)), 'id='.$id);
            echo (int)!$item['visible'];
            exit();
		}
	}

    private function set_main()
    {
        $id = Utils::parseGet('id');
        if ($id)
            $ret = $this->db->query("UPDATE news SET onmain = \\!onmain WHERE id = ?", array($id));
        echo json_encode(array('success' => isset($ret) && !PEAR::isError($ret)));
    }
}