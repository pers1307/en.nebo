<?
class News_media extends Module {

	protected $name = __CLASS__;

	private $limit = 10;

	private $folder = 'news_media/';
	private $path;
    private $link;


	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
	}

	function getContent() {
		$this->smarty->assign('media_link', $this->link);

        $this->page->doc['head'] = '';

		if (isset($_GET['n'])) {
			$this->show_item();
		} else {
			$this->show_list();
		}

		return true;
	}

	function show_list() {
        $p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;
        $d = (isset($_GET['d'])) ? Utils::rus2iso($_GET['d']) : false;

        $alldates = $this->db->getCol('SELECT DISTINCT DATE(FROM_UNIXTIME(publish_date)) as d FROM news_media WHERE visible = 1 ORDER BY publish_date');
        if (!PEAR::isError($alldates) && !empty($alldates)) {
            $alldates = json_encode($alldates);
        } else $alldates = array();

        $count = $this->db->getOne('SELECT COUNT(*) FROM news_media '.($d? 'WHERE DATE(FROM_UNIXTIME(publish_date))="'.$d.'"':''));
        $limit_from =  $this->limit * $p;
		$count = ceil($count / $this->limit);
		$this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

		$news_list = $this->db->getAll("SELECT * FROM news_media WHERE visible = 1 ".($d? 'AND DATE(FROM_UNIXTIME(publish_date))="'.$d.'"':'')." ORDER BY publish_date DESC LIMIT ?, ?", array($limit_from, $this->limit));
		if (!PEAR::isError($news_list) && !empty($news_list)) {
			foreach ($news_list as $k=>$row) {
				if ($row['img']) {
					$news_list[$k]['img_src'] = $this->folder.$row['id'].'_s.'.$row['img'];
				}
			}
		}
		if ($d) $this->smarty->assign('date', preg_replace('/[^0-9\.]/', '', $_GET['d']));
		$this->smarty->assign('media_list', $news_list);
		$this->smarty->assign('alldates', $alldates);
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'media_list.tpl');
	}

	function show_item() {
		$id = intval($_GET['n']);
		if (!$id) {
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'media_item.tpl');
			return;
		}
		$item = $this->db->getRow("SELECT * FROM news_media WHERE id = $id");
		if (!is_array($item) or !count($item)) {
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'media_item.tpl');
			return;
		}
		
		if ($item['img']) {
			$item['img_src'] = $this->folder.$item['id'].'.'.$item['img'];
		}

        $this->smarty->assign('link_back', $this->link);
		$this->smarty->assign('media_item', $item);
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'media_item.tpl');
	}

	function get_short_list($limit = 4) {
		$short_list = $this->db->getAll("SELECT id, title, short_text, publish_date FROM news_media ORDER BY publish_date DESC LIMIT {$limit}");
		return (is_array($short_list)) ? $short_list : false;
	}

}