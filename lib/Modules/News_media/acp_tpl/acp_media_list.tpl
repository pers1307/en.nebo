<div class="padding-block">
<h3>Список публикаций</h3>
<a class="btn btn-success" href="?doc_id={$doc_id}&module&action=edit">
	<i class="icon-plus icon-white"></i>
	Добавить новую
</a>
</div>
{if !empty($media)}
	<table class="table table-striped">
	 <tr>
	  <th>#</th>
	  <th>Заголовок</th>
         <th style="width:115px;">Дата публикации</th>
         <th style="width:30px;">&nbsp;</th>
         <th style="width:80px;">&nbsp;</th>
	 </tr>    
	{foreach from=$media item=media_item}
	 <tr id="tr_{$media_item.id}"{if $media_item.visible == 0} class="info muted"{/if}>
	  <td><a href="?doc_id={$doc_id}&module{if $smarty.get.p}&p={$smarty.get.p}{/if}&action=edit&id={$media_item.id}"{if $media_item.visible == 0} class="info muted"{/if}>{$media_item.id}</a></td>
	  <td><a href="?doc_id={$doc_id}&module{if $smarty.get.p}&p={$smarty.get.p}{/if}&action=edit&id={$media_item.id}"{if $media_item.visible == 0} class="info muted"{/if}>{$media_item.title}</a></td>
	  <td>{if ($media_item.publish_date)}{$media_item.publish_date|date_format:"%d.%m.%Y %H:%I"}{/if}</td>    
	  <td class="center"><span rel="visible" href="#" class="btn btn-mini{if !$media_item.visible} btn-info{/if}" onclick="return visibleItem({$media_item.id})"><i class="{if $media_item.visible}icon-eye-open{else}icon-eye-close icon-white{/if}"></i></span></td>
      <td class="center"><span onclick="news_delete('{$media_item.id}','{$media_item.title}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td> 
	 </tr>
	{/foreach}
	</table>
	
	{if ($pages.count > 1)}
	  <p class="pager">
	  {section name=pages loop=$pages.count start=0}
	    {assign var=pi value="`$smarty.section.pages.iteration - 1`"}
	    <a href="?doc_id={$doc_id}&module&p={$pi}" {if ($pi == $pages.current)}style="font-weight: bold;"{/if}>{$pi+1}</a>
	  {/section}
	  </p>
	{/if}

{else}
	<div class="padding-block"><p>Публикации не созданы</p></div>
{/if}

<script type="text/javascript">
function news_delete(id, title) {
	if (confirm('Удалить публикацию "'+title+'"?')) {
		$.post('?doc_id='+doc_id+'&module&action=delete', { id:id }, function(data) {
        	if (data == 1) {
				$('#tr_'+id).fadeOut('fast');
    		}
		} )
	}
}

visibleItem = function(i) {
    $.get('?doc_id={$doc_id}&module&id='+i+'&action=item_visibility', function(r){
        if (r==1) {
	        $('table #tr_'+i+' span[rel=visible]').removeClass('btn-info');
	        $('table #tr_'+i+' span[rel=visible] i').removeClass('icon-eye-close icon-white').addClass('icon-eye-open');
	        $('table #tr_'+i+' ').removeClass('info muted');
            $('table #tr_'+i+' td a').removeClass('muted');
        }
        else {
            $('table #tr_'+i+' span[rel=visible]').addClass('btn-info');
            $('table #tr_'+i+' span[rel=visible] i').addClass('icon-eye-close icon-white').removeClass('icon-eye-open');
            $('table #tr_'+i+' ').addClass('info muted');
            $('table #tr_'+i+' td a').addClass('muted');
        }
    });
}
</script>