<div class="news-page one-news">
    {if ($media_item)}

        <h3>{$media_item.title}</h3>
        {if (!$media_item.full_text)}{$media_item.short_text}{else}{$media_item.full_text}{/if}

        <br />
        <a href="/{$media_link}/">Back to the list</a>
    {else}
        <p>Item not found</p>
    {/if}
</div>
<script type="text/javascript">
    createPrettyPhoto();
</script>
