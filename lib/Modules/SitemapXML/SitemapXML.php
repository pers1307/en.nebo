<?
class SitemapXML extends Module {

    protected $name = __CLASS__;
	
	private $changefreq = array('monthly', 'weekly', 'daily', 'hourly', 'yearly', 'always', 'never');

	private $url;
	
	/**
	 * @var TreeMenu
	 */
	var $treeMenu;


	function getContent() {
		$this->url = 'http://'.$_SERVER['HTTP_HOST'];
		
		header("Content-type: text/xml; charset=utf8;");
		echo '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
		echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\r\n";
		$this->get_tree(0);
		echo '</urlset>';
	}


	function get_tree($parent) {
		$tree = $this->get_tree_childs($parent);
		if ($tree) {
			foreach ($tree as $item) {
				echo " <url>\r\n";
				echo "  <loc>{$this->url}{$item['fullpath']}</loc>\r\n";
                echo "  <priority>1</priority>\r\n";
				
				$changefreq = $item['changefreq'];
				if ($changefreq and (array_search($changefreq, $this->changefreq) !== false)) {
					echo "  <changefreq>{$changefreq}</changefreq>\r\n";
				}
				
				echo " </url>\r\n";
				
				$this->get_tree($item['id']);
			}
		}
	}


	private function get_tree_childs($parent) {
		$sql = "SELECT id, parent, fullpath, changefreq FROM tree WHERE parent = $parent AND publish_time < NOW() AND sitemap = 1 ORDER BY priority";
		$res = $this->db->getAll($sql);
		return (is_array($res) and count($res)) ? $res : false;
	}
	
}
?>