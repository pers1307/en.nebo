<?
class ACP_Leaders extends ACP_Module {

	protected $name = __CLASS__;

	private $folder = 'leaders/';
	private $path;

    private $img_size = array('img'=>array(400=>'', 250=>'_s'));

	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;
	}

	function rewrite() {
		switch ($this->action) {
            case 'group_visibility':
                $this->group_visibility();
                return false;
                break;

            case 'group_rename':
                $this->group_rename();
                return false;
                break;

            case 'sort_groups':
                $this->sort_groups();
                return false;
                break;

            case 'list':
                $this->show_list();
                break;

            case 'edit':
				$this->edit_leaders();
				break;

			case 'save':
				$this->save_leaders();
				break;

			case 'delete':
				$this->delete_leaders();
				return false;
				break;

            case 'upload_images':
                $this->upload_images(Utils::parseGet('id'), $this->img_size);
                return false;
                break;

            case 'img_delete':
				$this->delete_img();
				return false;
				break;

            case 'item_visibility':
				$this->item_visibility();
				return false;
				break;

            case 'sort':
                $this->sort();
                return false;
                break;

 			default:
 				$this->show_group();
 				break;
		}
		return true;
	}

	function fix_slashes() {
		$list = $this->db->getAll("SELECT id, full_text FROM leaders");
		foreach ($list as $row) {
			extract($row);
			$full_text = stripslashes($full_text);
			$this->db->update('leaders', compact('full_text'), "id = {$id}");
		}
	}

    private function show_group() {
        $list = $this->db->getAll("SELECT * FROM leaders_groups ORDER BY sort");

        $this->smarty->assign('groups', $list);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_leaders_groups.tpl');
    }

    private function group_rename() {
        if (!isset($_POST['id']) or !isset($_POST['title'])) exit;
        $id = intval($_POST['id']);
        $title = trim($_POST['title']);

        if (!$id or !$title) exit;

        $this->db->update('leaders_groups', compact('title'), "id = {$id}");
        echo 1;
    }

	private function show_list() {
        $id = Utils::parseGet('id');
		$list = $this->db->getAll("SELECT * FROM leaders WHERE parent = ".$id." ORDER BY sort");

        $this->smarty->assign('leaders', $list);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_leaders_list.tpl');
	}

	private function edit_leaders() {
		if ((isset($_GET['id'])) and ($id = intval($_GET['id']))) {
			$leaders = $this->db->getRow("SELECT * FROM leaders WHERE id = {$id}");



            foreach ($this->img_size as $name => $sizes) {
                foreach ($sizes as $prefix) {
                    $url = $id . $prefix . '.jpg';
                    if (file_exists($this->path . $url)) {
                            $leaders[$name] = $this->folder . $url;
                    }
                }
            }

            $this->smarty->assign('item', $leaders);

		}

        $groups = $this->db->getAll("SELECT * FROM leaders_groups ORDER BY sort");
        $this->smarty->assign('group', $groups);

        $this->smarty->assign('img_sizes', $this->img_size);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_leaders_edit.tpl');
	}

	private function save_leaders() {
		if (empty($_POST)) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module");
			exit;
		}
		$id = intval($_POST['id']);

		$arr = array();
		$arr['lastname'] = htmlspecialchars(trim($_POST['lastname']));
		$arr['firstname'] = htmlspecialchars(trim($_POST['firstname']));
		$arr['patronymic'] = htmlspecialchars(trim($_POST['patronymic']));
		$arr['position'] = htmlspecialchars(trim($_POST['position']));
		$arr['parent'] = htmlspecialchars(trim($_POST['group']));
		$arr['text'] = stripslashes(trim($_POST['full_text']));
		$arr['create_date'] = time();

		if (!$arr['lastname'] && !$arr['firstname'] && !$arr['patronymic'] && !$arr['parent']) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
			exit;
		}

		if ($id) {
			$this->db->update('leaders', $arr, "id = $id");
		} else {
			$id = $this->db->insert('leaders', $arr);
		}

        $this->upload_images($id, $this->img_size);

		header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
		exit;
	}

	private function upload_images($id, $key_arr = array()) {
		$output = array();
		foreach ($key_arr as $key=>$sizes) {
			if (!isset($_FILES[$key]) or empty($_FILES[$key]['tmp_name']) or $_FILES[$key]['error']) continue;

            $tmp_name = $_FILES[$key]['tmp_name'];
			$file_name = $_FILES[$key]['name'];

			$dest_path = $this->path;

			if (!file_exists($dest_path)) {
				Tools::create_directory($dest_path, 0777);
			}
			if (!is_writeable($dest_path)) continue;

			$e = strrpos($file_name, '.') + 1;
			if ($e == 1) continue;

			$ext = substr($file_name, $e);
			if ($ext == 'jpeg') $ext = 'jpg';

			if (!in_array($ext, array('jpg', 'png', 'gif'))) continue;

            // convert to jpg
            $ext = 'jpg';

			$file = $dest_path.$id.'.'.$ext;

			if (file_exists($file)) {
				unlink($file);
			}

			rename($tmp_name, $file);
			@chmod($file, 0755);

			if (is_array($sizes) and count($sizes)) {
				$Img = Image::CreateFromFile($file);
				foreach ($sizes as $w=>$prfx) {
					$new_file = $dest_path.$id.$prfx.'.'.$ext;
					$Img->resize($w);
					$Img->save($new_file, 'jpeg');
				}
			}

			$output['img'] = $ext;
		}
		return $output;
	}


	private function delete_leaders(){
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$this->db->delete('leaders', "id = $id");

		echo 1;
	}


	function delete_img() {
		if (!isset($_POST['id']) or !isset($_POST['imgkey'])) exit;
		$id = intval($_POST['id']);
		$imgkey = trim($_POST['imgkey']);

		if (!$id or !isset($this->img_size[$imgkey])) exit;

        $prfxs = $this->img_size[$imgkey];
        if (is_array($prfxs)) {
            foreach ($prfxs as $prfx) {
                $file = $this->path."{$id}{$prfx}.jpg";
                if (file_exists($file)) {
                    @unlink($file);
                }
            }
        } else {
            $file = $this->path."{$id}.jpg";
            if (file_exists($file)) {
                @unlink($file);
            }
        }
		echo 1;
	}

    public function group_visibility() {
        $id = Utils::parseGet('id');
        $item = $this->db->getRow("SELECT * FROM leaders_groups WHERE id=".(int)$id);
        if (!empty($item)) {
            $this->db->update('leaders_groups', array('visible'=>($item['visible'] ? 0:1)), 'id='.$id);
            echo (int)!$item['visible'];
            exit();
        }
    }

    public function item_visibility() {
		$id = Utils::parseGet('id');
		$item = $this->db->getRow("SELECT * FROM leaders WHERE id=".(int)$id);
		if (!empty($item)) {
			$this->db->update('leaders', array('visible'=>($item['visible'] ? 0:1)), 'id='.$id);
            echo (int)!$item['visible'];
            exit();
		}
	}

    private function sort_groups()
    {
        if (!empty($_POST['ids'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->simpleQuery("
            UPDATE leaders_groups SET sort = (@sort:=@sort + 1)
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ");
            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

    private function sort()
    {
        if (!empty($_POST['ids']) && !empty($_GET['id'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->query("
            UPDATE leaders SET sort = (@sort:=@sort + 1)
            WHERE parent = ?
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ", array($_GET['id']));
            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

}