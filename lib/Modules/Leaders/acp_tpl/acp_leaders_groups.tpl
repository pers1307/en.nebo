<div class="padding-block">
    <h3>Группы руководителей</h3>
</div>
{if !empty($groups)}
    <table class="table table-striped">
        <thead>
        <tr>
            <th style="width: 30px;">#</th>
            <th>Группа</th>
            <th style="width: 150px;">&nbsp;</th>
            <th style="width: 80px;">&nbsp;</th>
        </tr>
        </thead>
        <tbody id="sortable">
        {foreach $groups as $item}
            <tr id="tr_{$item.id}"{if $item.visible == 0} class="info muted"{/if} data-id="{$item.id}">
                <td><a href="?doc_id={$doc_id}&module&action=list&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.id}</a></td>
                <td class="_title"><a href="?doc_id={$doc_id}&module&action=list&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.title}</a></td>
                <td class="center">
                    <span href="#" class="btn btn-success btn-mini" data-toggle="modal" data-target="#renameModal" onclick="itemId='{$item.id}'; $('#renameModal #title').val($('#tr_{$item.id} td._title a').text());"><i class="icon-pencil icon-white"></i> Переименовать</span>
                    <span rel="visible" href="#" class="btn btn-mini{if !$item.visible} btn-info{/if}" onclick="return visibleGroup({$item.id})"><i class="{if $item.visible}icon-eye-open{else}icon-eye-close icon-white{/if}"></i></span>
                </td>
                <td class="center"><span onclick="leaders_delete('{$item.id}','{$item.title}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <div class="padding-block"><p>Руководители не созданы</p></div>
{/if}

{* MODAL RENAME *}

<div id="renameModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Изменение названия</h3>
    </div>
    <div class="modal-body">
        <form class="form-inline" id="editAlbumForm">
            <input type="text" name="title" id="title" class="input-block-level" placeholder="Текущее название альбома">
            <span class="fl_callback"></span>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
        <button class="btn btn-primary" onclick="renameFromModal();">Сохранить изменения</button>
    </div>
</div>

{* END MODAL *}

{* onclick="fileId='{$file.id}'; $('#renameModal #title').val($('div#original-title-{$file.id}').text()); $('#renameModal #description').val($('div#original-description-{$file.id}').text());" *}

<script type="text/javascript">
    function leaders_delete(id, title) {
        if (confirm('Удалить "'+title+'"?')) {
            $.post('?doc_id='+doc_id+'&module&action=delete', { id:id }, function(data) {
                if (data == 1) {
                    $('#tr_'+id).fadeOut('fast');
                }
            } )
        }
    }

    var itemId = '';

    function renameFromModal() {

        $.post('?doc_id={$doc_id}&module&action=group_rename', { id:itemId, title:$('#renameModal #title').val() }, function(data) {
            if (data == 1) {
                $('#tr_'+itemId+' td._title a').text($('#renameModal #title').val());
                $('#renameModal').modal('hide');
            } else {
                $('.fl_callback').html(data);
            }
        });
    }

    visibleGroup = function(i) {
        $.get('?doc_id={$doc_id}&module&id='+i+'&action=group_visibility', function(r){
            if (r==1) {
                $('table #tr_'+i+' span[rel=visible]').removeClass('btn-info');
                $('table #tr_'+i+' span[rel=visible] i').removeClass('icon-eye-close icon-white').addClass('icon-eye-open');
                $('table #tr_'+i+' ').removeClass('info muted');
                $('table #tr_'+i+' td a').removeClass('muted');
            }
            else {
                $('table #tr_'+i+' span[rel=visible]').addClass('btn-info');
                $('table #tr_'+i+' span[rel=visible] i').addClass('icon-eye-close icon-white').removeClass('icon-eye-open');
                $('table #tr_'+i+' ').addClass('info muted');
                $('table #tr_'+i+' td a').addClass('muted');
            }
        });
    }

    $("#sortable").sortable({
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        axis: 'y',
        delay: 100,
        stop: function() {
            var data = { ids:[] };
            $('#sortable tr').each(function(i, el){
                data.ids[i] = $(el).data('id');
            });
            $.post('?doc_id={$doc_id}&module&action=sort_groups', data);
        }
    }).disableSelection();
</script>