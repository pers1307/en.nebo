<?
class Leaders extends Module {

	protected $name = __CLASS__;

	private $folder = 'leaders/';
	private $path;
    private $link;


	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
	}

	function getContent() {

		$this->smarty->assign('leaders_link', $this->link);

        $this->page->doc['head'] = '';

		if (isset($_GET['n'])) {
			$this->show_item();
		} else {
			$this->show_list();
		}

		return true;
	}

	function show_list() {
		//$list = $this->db->getAll("SELECT * FROM leaders WHERE visible = 1 ORDER BY sort");
        $groups = $this->db->getAll("SELECT * FROM leaders_groups WHERE visible = 1 ORDER BY sort");


        if (!PEAR::isError($groups) && !empty($groups)) {
            foreach ($groups as $gk=>$gv) {
                $q = "SELECT * FROM leaders WHERE parent = {$gv['id']} AND visible = 1 ORDER BY sort";
                $groups[$gk]['items'] = $this->db->getAll($q);

                foreach ($groups[$gk]['items'] as $k=>$row) {
                    $url = $row['id'].'_s.jpg';
                    if (file_exists($this->path . $url))
                        $groups[$gk]['items'][$k]['img'] = $this->folder . $url;
                }



            }
        }

        //Utils::dmp($groups[0]);
		$this->smarty->assign('leaders_list', $groups);
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'leaders_list.tpl');
	}

	function show_item() {
		$id = intval($_GET['n']);
		if (!$id) {
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'leaders_item.tpl');
			return;
		}
		$item = $this->db->getRow("SELECT * FROM leaders WHERE id = $id");
		if (!is_array($item) or !count($item)) {
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'leaders_item.tpl');
			return;
		}


        $url = $item['id'].".jpg";
        if (file_exists($this->path . $url)) {
            $item["img"] = $this->folder . $url;
        }

        $this->smarty->assign('link_back', $this->link);
		$this->smarty->assign('leaders_item', $item);
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'leaders_item.tpl');
	}

}