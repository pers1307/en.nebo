<?php
//	protected $name = __CLASS__;

require_once("../lib/Core/Vars.php");
require_once R_core.'MyAuth.php';
//-- AUTOLOAD -----------------------------------
spl_autoload_register ('autoload');
function autoload ($className) {
	$fileName = R_core.$className.'.php';
	if (file_exists($fileName)) {
		require_once($fileName);
	}
}
//-----------------------------------------------
/**/

$Auth = new MyAuth();
$res = $Auth->startAuth(false);
if (!$res) exit;

$db = MyDB2::GetInstance();
SmartyHolder::GetInstance(R_tpl_acp);

require_once R_Modules."/Gallery/ACP_Gallery.php";

$APrjct = new ACP_Gallery();
$APrjct->MultiUpload();