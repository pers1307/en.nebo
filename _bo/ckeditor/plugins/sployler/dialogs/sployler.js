CKEDITOR.dialog.add('sployler', function(editor) {
	return {
		title : 'Скрыть в сплойлер',
		minWidth : 400,
		minHeight : 200,
		onOk: function() {
//			var sel = new CKEDITOR.dom.selection(CKEDITOR.document);
//			var text = editor.getSelection().getSelectedText();
			var selection = editor.getSelection().getNative();
			var sployler_text = this.getContentElement('sployler_tab', 'sployler_title').getInputElement().getValue();
			var sployler_style = this.getContentElement('sployler_tab', 'sployler_style').getInputElement().getValue();
			var html = '<div class="sployler '+sployler_style+'"><span class="title">'+sployler_text+'</span><div class="fulltext">'+selection+'</div></div><br />&nbsp;';
			this._.editor.insertHtml(html);
		},
		contents : [{
			id: 'sployler_tab',
			label: 'First Tab',
			title: 'First Tab',
			elements: [
				{
					id: 'sployler_title',
					type: 'text',
					label: 'Заголовок',
					validate: CKEDITOR.dialog.validate.notEmpty('Укажите заголовок'),
					required: true
				},
				{
					id: 'sployler_style',
					type: 'select',
					label: 'Стиль',
					'default' : '',
					items:	[
						['с рамкой', ''],
						['без рамки', 'no-border'],
					],
				}
			]
		}]
	};
});