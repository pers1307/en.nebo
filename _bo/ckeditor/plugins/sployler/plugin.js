CKEDITOR.plugins.add('sployler', {
	init : function(editor) {
		var command = editor.addCommand('sployler', new CKEDITOR.dialogCommand('sployler'));
		command.modes = {wysiwyg:1, source:1};
		command.canUndo = true;

		editor.ui.addButton('sployler', {
			label : 'Скрыть в сплойлер',
			command : 'sployler',
			icon: this.path + 'images/sployler.gif'
		});

		CKEDITOR.dialog.add('sployler', this.path + 'dialogs/sployler.js');
	}
});