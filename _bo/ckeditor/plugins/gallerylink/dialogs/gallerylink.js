CKEDITOR.dialog.add('gallerylink', function(editor) {
	var get_gallerylinks = function(folder_id, ck_name) {
		if (!folder_id) folder_id = 0;
		$.post('/_bo/?action=galleries', {act:'get_content', folder_id:folder_id, ck_name:ck_name}, function(data) {
			$('td.cke_dialog_ui_vbox_child div.cke_dialog_ui_html').html(data);
		});
	}


	this.get = function(folder_id) {
		get_gallerylinks(folder_id);
	}

	return {
		title : 'Вставить галерею',
		width : 640,
		minWidth : 640,
		minHeight : 200,

		contents : [{
			id : 'info',
			label : 'Галереи',
			title : 'Галереи',
			elements : [
                {
                    type : 'html',
                    id : 'break',
                    style : 'width: 100%; clear: both; height: 192px; overflow-y: auto; overflow-x: hidden;',
                    html : '<div></div>'
                },
                {
                    type: 'textarea',
                    id: 'code_galleries',
                    hidden: true
                }
			]
		}],

		onShow : function() {
			get_gallerylinks(0, this._.editor.name);
		},

		onOk: function() {
			var code_text = this.getContentElement('info', 'code_galleries').getInputElement().getValue();
			if (code_text) {
				this._.editor.insertHtml(code_text);
			}
			return true
		}
	};

});

var ck_get_gallerylink = CKEDITOR.tools.addFunction(function(folder_id, ck_name) {
	if (!folder_id) folder_id = 0;
	$.post('/_bo/?action=galleries', {act:'get_content', folder_id:folder_id, ck_name:ck_name}, function(data) {
		$('td.cke_dialog_ui_vbox_child div.cke_dialog_ui_html').html(data);
	});
});

var ck_add_link = function() {
	var html = ''
	$('#galleries_widget input.checkbox').each(function() {
		if ($(this).prop('checked') == true) {
			var id = $(this).attr('id');
			id = parseInt(id.replace('fl_check_', ''));
			var link = $('#fl_link_'+id).val();
			var title = $('#fl_title_'+id).val();
			var ext = $('#fl_ext_'+id).val();
			html += '<a href="'+link+'" class="file '+ext+'"><span>'+title+'</span></a>\r\n';
		}
	})
	if (html.lenght) {
		html += '<br />';
	}
	$('.cke_dialog_contents textarea.cke_dialog_ui_input_textarea').val(html);
}

var ck_insert_gallery = function(id, ck_name) {
	var title = $('#fl_title_'+id).val();
	html = '<a href="#" rel="gallery/'+id+'"><span>[Галерея: '+title+']</span></a>\r\n';
	var element = CKEDITOR.dom.element.createFromHtml(html);
	CKEDITOR.instances[ck_name].insertElement(element);
}
