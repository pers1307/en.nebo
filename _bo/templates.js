CKEDITOR.addTemplates( 'default',
{
	// The name of sub folder which hold the shortcut preview images of the templates.
	imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates :
		[
			{
				title: '2 колонки',
				description: 'Шаблон страницы в 2 колонки',
				html:
					'<table class="cols-2"><tr>'+
						'<td><p>Первая колонка</p></td>'+
						'<td><p>Вторая колонка</p></td>'+
					'</tr></table>'
			},
            {
                title: '3 колонки',
                description: 'Шаблон страницы в 3 колонки',
                html:
                    '<table class="cols-3"><tr>'+
                        '<td><p>Первая колонка</p></td>'+
                        '<td><p>Вторая колонка</p></td>'+
                        '<td><p>Третья колонка</p></td>'+
                        '</tr></table>'
            },
            {
                title: '4 колонки',
                description: 'Шаблон страницы в 4 колонки',
                html:
                    '<table class="cols-4"><tr>'+
                        '<td><p>Первая колонка</p></td>'+
                        '<td><p>Вторая колонка</p></td>'+
                        '<td><p>Третья колонка</p></td>'+
                        '<td><p>Четвертая колонка</p></td>'+
                        '</tr></table>'
            }
		]

		
});