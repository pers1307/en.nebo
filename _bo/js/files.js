// File Library

var acp_files_input_limit = 10;
var acp_files_input_count = 1;

function add_acp_files_input() {
	if (acp_files_input_count < acp_files_input_limit) {
		var html = ''
		html += '<div class="input_file" class="well">'
		html += '<input type="file" name="file[]" placeholder="Выберите файл" /> <div class="input-append"><input type="text" class="text" name="file_title[]" placeholder="Укажите название файла" autocomplete="off" /><button class="btn" type="button" onclick="acp_remove_files_input($(this))"><i class="icon-minus-sign"></i></button></div>'
		html += '</div>'
		$('#input_files').append(html)
		acp_files_input_count++
	}
}

function acp_remove_files_input(je) {
	je.parent().parent().remove();
	acp_files_input_count--;
}

function acp_show_file_link(id) {
	acp_file_update_link(id);
	$('#tr_'+id+' textarea').toggle();
}

function filelibrary_create_folder() {
	$('.fl_callback').html('');
	var title = window.prompt('Введите имя директории:');
	if ((!title) || (title == undefined)) return;
	var folder_id = $('#folder_id').val();
	$.post('/_bo/?action=filelibrary', {act:'create_folder', folder_id:folder_id, title:title}, function(data) {
		if (data == 1) {
			location = location;
		} else {
			$('.fl_callback').html(data);
		}
	})
}

function acp_file_update_link(id) {
	var link = $('#tr_'+id+' .link').val();
	var ext = $('#tr_'+id+' input.ext').val();
	var title = $('#tr_'+id+' .title').html();
	link = link+'/'+title+'.'+ext;
	var str = '';
//	str += '<div class="files">';
	str += '<a href="'+link+'" class="file '+ext+'"><span>'+title+'</span></a>';
//	str += '</div>';
	$('#tr_'+id+' textarea').html(str);
}

function acp_file_delete(id, title) {
	if (confirm('Удалить файл "'+title+'"?')) {
		$('.fl_callback').html('');
		$.post('/_bo/?action=filelibrary', {act:'delete_file', id:id}, function(data) {
			if (data == 1) {
				$('#tr_'+id).fadeOut();
			} else {
				$('.fl_callback').html(data);
			}
		})
	}
}

function acp_file_rename(id) {
	var old_title = $('#tr_'+id+' .title').html();
	var title = window.prompt('Введите новое имя:', old_title);
	if ((!title) || (title == undefined)) return;
	$('.fl_callback').html('');
	$.post('/_bo/?action=filelibrary', {act:'rename_file', id:id, title:title}, function(data) {
		if (data == 1) {
			$('#tr_'+id+' .title').html(title);
			acp_file_update_link(id);
		} else {
			$('.fl_callback').html(data);
		}
	})
}

// galleries

function galleries_create_folder() {
    $('.fl_callback').html('');
    var title = window.prompt('Введите имя галереи:');
    if ((!title) || (title == undefined)) return;
    var folder_id = $('#folder_id').val();
    $.post('/_bo/?action=galleries', {act:'create_folder', folder_id:folder_id, title:title}, function(data) {
        if (data == 1) {
            document.location.reload(true);
        } else {
            $('.fl_callback').html(data);
        }
    })
}

function acp_gallery_delete(id, title) {
    //if (confirm('Удалить файл "'+title+'"?')) {
        $('.fl_callback').html('');
        $.post('/_bo/?action=galleries', {act:'delete_file', id:id}, function(data) {
            if (data == 1) {
                $('#tr_'+id).fadeOut();
            } else {
                $('.fl_callback').html(data);
            }
        })
    //}
}

function acp_gallery_rename(id) {
    var old_title = $('#tr_'+id+' .title').html();
    var title = window.prompt('Введите новое имя:', old_title);
    if ((!title) || (title == undefined)) return;
    $('.fl_callback').html('');
    $.post('/_bo/?action=galleries', {act:'rename_file', id:id, title:title}, function(data) {
        if (data == 1) {
            $('#tr_'+id+' .title').html(title);
            acp_file_update_link(id);
        } else {
            $('.fl_callback').html(data);
        }
    })
}
