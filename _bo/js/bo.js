function img_preview(e, url) {
	e.parent().append('<img class="preview" onmouseout="$(this).remove()" src="'+url+'" />')
}

function rand(min, max) {
	return parseInt(Math.random() * (max - min) + min)
}

function set_star(id, je, field) {
	$.post('?doc_id='+doc_id+'&module&action=set_star', {id:id, field:field}, function(data) {
//		data = parseInt(data)
		if (data == '1') {
			je.removeClass('icon-eye-close')
			je.addClass('icon-eye-open')
		} else if (data == '0') {
			je.removeClass('icon-eye-open')
			je.addClass('icon-eye-close')
		}
	})
}

function refresh_modules() {
	var selected = $('select#selcet_module').val()
	$.post('/_bo/?action=update_load_modules', {selected:selected}, function(data) {
		if (data) {
			$('select#selcet_module').html(data)
		}
	})
}

function switch_page_view_mode() {
	var mode = $('#bo_tree').css('display')
	if (mode == 'none') {
		$('#bo_tree').show()
		$('.main td.space30').css('width', '30px')
	} else {
		$('#bo_tree').hide()
		$('.main td.space30').css('width', '2px')
	}
}

function show_file_library() {
	var visible = $('#bo_tree .file_library').css('display')
	if (visible == 'none') {
		$('#bo_tree .file_library').fadeIn()
		get_file_library('')
	} else {
		$('#bo_tree .file_library').fadeOut()
	}
}

function get_file_library(path) {
	$.post('/_bo/?action=file_library', {f:path}, function(data) {
		$('#bo_tree .file_library').html(data)
	})
}


function saveDoc(category) {
	var id = $('#doc_id').val();
	var match = false;
	var post_data = {'id':id};
	if (category == 'text') {
		match = true;
		post_data['body_top'] = CKEDITOR.instances.body_top.getData();
		post_data['body'] = CKEDITOR.instances.body.getData();
	}
	if (category == 'seo') {
		match = true;
		post_data['title'] = $('#title').val();
		post_data['title_h1'] = $('#title_h1').val();
		post_data['meta_description'] = $('#meta_description').val();
		post_data['meta_keywords'] = $('#meta_keywords').val();
		post_data['changefreq'] = $('#changefreq').val();
        post_data['sitemap'] = $('#sitemap').prop('checked');
	}
	if (category == 'options') {
		match = true;
		post_data['head'] = $('#head').val();
		post_data['fname'] = $('#fname').val();
		post_data['redirect'] = $('#redirect').val();
		post_data['module'] = $('#selcet_module').val();
		post_data['menushow'] = $('#menushow').prop('checked');
	}

	if (match) {
		post_data['category'] = category;
		$('#de_'+category+' .b-save').prop('disabled', 'disabled');
		$('#de_'+category+' .b-save').addClass('act');
		$('#de_'+category+' .b-save').val('');
		$.post('?action=saveDoc', post_data, function(data) {
			if (data) {
				$('.callback').addClass('alert-'+data['type']).html('<h4>'+data['title']+'</h4><p>'+data['answer']+'</p>');
				$('.callback').fadeIn('fast');
				$('#de_'+category+' .b-save').removeClass('act');
				$('#de_'+category+' .b-save').prop('disabled', '');
				$('#de_'+category+' .b-save').val('Сохранить');

                if (data['module'] == 1) {
					$('#pt_module').fadeIn();
				}
			} else {
                $('.callback').addClass('alert-error').html('<h4>Ошибка</h4><p>Страница не сохранена по неизвестной причине</p>');
			}
			setTimeout("$('.callback').fadeOut('slow')", 4000);

		}, "json")
	}
}

function del(id) {
	if (confirm("Удалить текущий документ со всеми потомками?")) {
		document.location = "?action=deleteDoc&id=" + id + "";
	} else {
		return;
	}
}

function pagePreview() {
	f = document.forms.editorSave;
	if (!f) return false;
	oldtarget = f.target;
	oldaction = f.action;
	f.target = "_blank";
	f.action = "?action=previewDoc";
	f.submit();
	f.target = oldtarget;
	f.action = oldaction;
}

function setRedirect(uri) {
	document.forms.editorSave.redirect.value=uri;
}

function digit_only(event) {
	if (window.event) event = window.event;
	var key = event.keyCode ? event.keyCode : event.which ? event.which : null
	if (((key < 48) || (key > 57)) && (key != 8) && (key != 9) && (key != 46) && (key != 37) && (key != 39)) return false;
	return true;
}


function toggle_sortet() {
	if ($('.tree-site').hasClass('disable_sort')) {
		$('.tree-site').removeClass('disable_sort')
		$(".tree-site").disableSelection()
	} else {
		$('.tree-site').addClass('disable_sort')
		$(".tree-site").enableSelection()
	}
}

function save_tree_sort(item_id, parent_id) {
	var sort = ''
	$('.tree-site #'+parent_id+' > li').each(function() {
		sort += $(this).attr('id')+';'
	})
	$.post('?action=save_tree_sort', {item_id:item_id, parent_id:parent_id, sort:sort}, function(data) {
//		console.log(data)
		item_id = parseInt(item_id.replace('tree_item_', ''))
		if (item_id == doc_id) {
			$('a#site_page_link').attr('href', data)
		}
	})
}

$(function(){

	$(".tree-site").sortable({
		placeholder: "placeholder",
		handle:'span.drag',
		items:'li.movable',
		stop: function(event, ui) {
			console.log(ui.item);
			var item_id = ui.item.attr('id');
			var parent_id = ui.item.parent().attr('id');
			save_tree_sort(item_id, parent_id);
		}
	})
})
